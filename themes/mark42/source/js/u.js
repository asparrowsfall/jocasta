(function($) {

    window.auctions = {};
    //UTIL: Debounce
    function debounce(fn, delay) {
        var timer = null;
        return function() {
            var context = this,
                args = arguments;
            clearTimeout(timer);
            timer = setTimeout(function() {
                fn.apply(context, args);
            }, delay);
        };
    }

    //ISO setup
    var $grid = $('.all-auctions');
    $grid.isotope({
        itemSelector: '.auction',
        layoutMode: 'fitRows',
        getSortData: {
            creator: '[data-creator]',
            bid: function(itemElem) {
                return parseInt(window.auctions[$(itemElem).attr('id')]) || 0;
            }
        },
        sortBy: 'creator'
    });

    // store filter for each group
    var filters = {};
    var sorts = {};
    var typeahead = $("#type-ahead");

    //Filter handler
    $('.filters').on('click', '.button', combinedFilters);
    //Search handler
    typeahead.keyup(debounce(search, 400));

    function search() {
        if ($(this).val() == '') {
            return $grid.isotope({ filter: '' });
        }
        return $grid.isotope({
            filter: function() {
                return $(this).attr('id').indexOf(typeahead.val().toLowerCase()) !== -1;
            }
        });
    }

    //Handler functions
    function combinedFilters() {
        var $this = $(this);

        // get group key
        var $buttonGroup = $this.parents('.button-group-filter');
        var filterGroup = $buttonGroup.attr('data-filter-group');
        // set filter for group
        if (filterGroup) {
            filters[filterGroup] = $this.attr('data-filter');
        }
        // combine filters
        var filterValue = concatValues(filters);
        // set filter for Isotope
        $grid.isotope({ filter: filterValue });
    }

    // change is-checked class on buttons
    $('.button-group').each(function(i, buttonGroup) {
        var $buttonGroup = $(buttonGroup);
        $buttonGroup.on('click', 'button', function() {
            $buttonGroup.find('.is-checked').removeClass('is-checked');
            $(this).addClass('is-checked');
        });
    });

    // flatten object by concatting values
    function concatValues(obj) {
        var value = '';
        for (var prop in obj) {
            value += obj[prop];
        }
        return value;
    }

    $.ajax({
        url: "https://script.google.com/macros/s/AKfycbwyTOafXNd8L_4rCS-kjyGm7UrYtA63m5cy7eIKS8HUR_nJK9k/exec?sheet=all",
        dataType: "jsonp",
        jsonpCallback: "assemble"
    }).then(function(auctions) {
        window.auctions = auctions;
        for (var auction in auctions) {
            var bid = auctions[auction];
            var auctionPanel = $('#' + auction);
            auctionPanel
                .find(".currentBid")
                .removeClass("hidden")
                .find("span")
                .text(bid);
            auctionPanel.attr('data-bid', bid);
        }
        //Resort with updated data
        $grid.isotope('updateSortData').isotope();
        //Handler for sorting
        $('.button-group-sort .button').click(function() {
            $grid.isotope({ sortBy: $(this).data('filter') });
        });
        //Make the sort button available
        $('.ui-sort').removeClass('hidden');
    });




})(jQuery);
