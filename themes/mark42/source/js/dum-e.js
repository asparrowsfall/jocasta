$(function() {
    $.ajax({
        url: dumE.API_BASE+"?sheet=" + avenger + "--" + lot,
        dataType: "jsonp",
        jsonpCallback: "assemble"
    }).then(function(response) {
        $('.currentbid span').html('<span>$</span>' + response.latestBid + ' <small>('+ response.name+')</small>');
        $('.increment').html(response.bidIncrement);
        $('#bid').val(parseInt(response.latestBid) + response.bidIncrement);
        $('#bid').attr('min', parseInt(response.latestBid) + response.bidIncrement);
        $('#bid').attr('max', parseInt(response.latestBid) + 500);
        $('#bid').attr('step', parseInt(response.bidIncrement));
    });

    function clearValidity(input) {
        $('.showErrors').removeClass('.showErrors');
        $(input)
            .parent('.formControl')
            .find('.errors')
            .html('');
    }

    function checkCustomValidity(input) {
        clearValidity(input);
        var errorText;
        if (!input.checkValidity()) {
            if (input.validity.valueMissing) {
                errorText = 'This field cannot be left blank.'
            }
            if (input.type === "email" && input.validity.typeMismatch) {
                errorText = 'Please enter a valid email address';
            }

            if (input.name === 'bid') {
                if (input.validity.stepMismatch) {
                    errorText = 'Please enter a bid in increments of ' + input.step + ' dollars.';
                }
                if (input.validity.rangeUnderflow) {
                    errorText = input.value + ' is below the minimum bid of ' + input.min + '. Please enter a bid of at least ' + input.min;
                }
                if (input.validity.rangeOverflow) {
                    errorText = 'We can only accept bids that'+
                    ' are $500 more than the current bid via the site.'+
                    ' If you are, in fact, a Genius Billionaire Playperson Philanthropist, send us an '+
                    '<a href=\"mailto:stonytrumpshate+bidding@gmail.com\">e-mail</a> and we\'ll add your bid to the auction manually. Thank you! <3';
                }
            }

            $(input)
                .parent('.formControl')
                .addClass('showErrors')
                .find('.errors')
                .html(errorText);
        }
    }

    function submit(e) {
        e.preventDefault();
        var $form = $('#form');
        var serializedData = $form.serialize();
        var valid = $form.get(0).checkValidity();

        if (!valid) {
            $form.addClass('showErrors');
            $('.formControl input').each(function(i, input) {
                checkCustomValidity(input);
            })
            return;
        }

        $(".article-entry").html("<p>Submitting bid...</p><p>It can take up to 5 seconds to submit your bid. If it's been longer than 10 seconds, fill out the <a href=\"https://goo.gl/forms/MXM6m8gbFPI0tZn23\">support form</a> to let us know what went wrong. Thanks!</p>")

        $.ajax({
            url: dumE.postURL,
            type: "POST",
            data: serializedData
        }).then(function(response) {
            if (response.status === 'success') {
                $(".article-entry").html("<h2>Thank you for placing a bid!</h2><p>Your bid has been successfully submitted.</p><a href='" + allAuctionURL + "'>Click here to return to all auctions</a>");
            } else if (response.status === 'outbid') {
                $(".article-entry").html("<h2>You were outbid!</h2><p>" + response.message + "</p>");
            } else {
                $(".article-entry").html("<h2>Uh-oh, something went wrong.</h2><p>" + response.message + "</p>");
            }
        });
    }

    $("#submitButton").click(submit);
    $(".formControl input").on('focus', function() {
        clearValidity(this);
    });
    $(".formControl input").on('blur', function() {
        checkCustomValidity(this);
    });

});
