(function() {
    window.removalClass = window.removalClass || '.article';
    console.log(window.removalClass);
    var isStaging = window.location.href.indexOf("staging") !== -1;
    var startTime = moment.tz("2017-05-21 00:00:00", "America/New_York");
    var endTime = moment.tz("2017-05-27 23:59:59", "America/New_York");

    if (moment.tz() > endTime && !isStaging) {
        //--------Auction has ended
        $('#auction-ended').removeClass('hidden');
        $('#coming-soon').remove();
        return $(removalClass).remove();
    }

    if (moment.tz() > startTime && !isStaging) {
        //--------Auction is going on right now
        $('#auction-ended').remove();
        return $('#coming-soon').remove();
    }

    //--------Auction hasn't started
    $('#coming-soon').removeClass('hidden');
    $('#auction-ended').remove();
    return $(removalClass).remove();

})();
