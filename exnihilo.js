'use strict'

const mainURL = "http://auctions.stevetony.gives";

const fs = require('fs');
const request = require('request');
const workTypes = require('./source/_data/workTypes');
const YAML = require('yamljs');
let empties = [];

const cleanNames = {
    participantsMayDonateToAnyOfTheMainSthCharitiesInAdditionToTheOnesIAmListingHere: 'allCharities',
    emailAddress: 'email',
    iDoWantToCreateWorksThatContain: 'doWant',
    iDoNotWantToCreateWorksThatContain: 'doNotWant',
    universesIWillCreateFor: 'universes',
    linkToMyExistingWorks1: 'link1',
    linkToMyExistingWorks2: 'link2',
    linkToMyExistingWorks3: 'link3',
    iWantToMentionSpecificCharitiesInMyListings: 'hasCustomCharities'
};

//=====UTIL FUNCTIONS
//coerce 'yes' to bools
function makeBool(obj, prop) {
    if (!!obj[prop]) {
        obj[prop] = obj[prop].toLowerCase() === 'yes';
    }
}

const rmDir = function(dirPath) {
    try {
        var files = fs.readdirSync(dirPath);
    } catch (e) {
        return;
    }
    if (files.length > 0)
        for (var i = 0; i < files.length; i++) {
            var filePath = dirPath + '/' + files[i];
            if (fs.statSync(filePath).isFile())
                fs.unlinkSync(filePath);
            else
                rmDir(filePath);
        }
};

//throw some leading zeroes on the IDs
function pad(num, size) {
    let s = num + "";
    while (s.length < size) s = "0" + s;
    return s;
}

//JSONP Callback
function assemble(json) {
    return json;
}

function exnihilo(body) {
    const data = eval(body);

    rmDir('./source/_posts/');

    let auctions = data.works;
    let { creators } = data;

    console.log(auctions);

    //PREP
    createPosts(auctions, creators);

    //Write reference files for debugging
    fs.writeFileSync('./auctions-all.json', JSON.stringify(auctions, null, 4));
    fs.writeFileSync('./creators-all.json', JSON.stringify(creators, null, 4));

    //Tumblr Links Lists for Blogs

    let creatorsSorted = creators.sort((a, b) => {
        var textA = a.creatorName.toUpperCase();
        var textB = b.creatorName.toUpperCase();
        return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
    });

    let linksArr = creatorsSorted.map(creator => !!creator.tumblrUrl ? `<li><a href="${creator.tumblrUrl}" target="_blank">${creator.creatorName}</a></li>` : `<li>${creator.creatorName}</li>`);
    let linksBlockArr = creatorsSorted.map(creator => !!creator.tumblrUrl ? `<a href="${creator.tumblrUrl}" target="_blank">${creator.creatorName}</a> |` : `${creator.creatorName} |`);
    let noLinksArr = creatorsSorted.map(creator => `<li>${creator.creatorName}</li>`);
    let linksString = `<ul>${linksArr.join("")}</ul>`
    let linksBlockString = `${linksBlockArr.join(" ")}`
    let noLinksString = `<ul>${noLinksArr.join("")}</ul>`

    fs.writeFileSync('./creators-link-list.html', linksString);
    fs.writeFileSync('./creators-link-list-block.html', linksBlockString);
    fs.writeFileSync('./creators-nolink-list.html', noLinksString);

    //AUCTION Links Lists for Reference

    let aucLinksArr = auctions.map(auction => {
        return `<li><a href="http://auctions.stevetony.gives/${auction.creatorTag}/${auction.idString}/" target="_blank">${auction.idString} - ${auction.creatorName} - ${auction.workType.desc}</a></li>`
    });
    let aucLinksString = `<ul>${aucLinksArr.join("")}</ul>`

    fs.writeFileSync('./auctions-link-list.html', aucLinksString);

    //== Status report out:
    console.log("  ");
    console.log("Files created. " + creators.length + " creators with " + auctions.length + " auctions found.")
    console.log("  ");
    console.log("Folks with no auctions:");
    console.log(empties.join(", "));
    console.log("  ");
}

function createPosts(auctions, creators) {
    creators.forEach(creator => {
        for (const prop in cleanNames) {
            creator[cleanNames[prop]] = creator[prop];
            delete creator[prop];
        }
        //coerce bool
        makeBool(creator, 'allCharities');
        makeBool(creator, 'hasCustomCharities');
        creator.creatorTag = creator.creatorName.toLowerCase().replace(/[\s\n\r]/g, "").replace(/[_\&\/]/g, "-").replace(/--/g, "-");
    });
    auctions.forEach(auction => {
        let aucCreator = creators.find(creator => {
            return creator.creatorName.toLowerCase() == auction.creatorName.toLowerCase();
        });
        if (!aucCreator) {
            console.log(`WARNING: ${auction.id} doesn't have a creator? It should be something like: ${auction.creatorName}`)
        }
        if (!!aucCreator && !!aucCreator.tumblrUrl) {
            auction.tumblrUrl = aucCreator.tumblrUrl
        } else {
            auction.tumblrUrl = mainURL;
        }
        auction.creatorTag = aucCreator.creatorTag;
        auction.timestamp = new Date(auction.timestamp);
        auction.workType = workTypes.find(function(kind) {
            return kind.desc == auction.workType;
        });
        auction.idString = pad(auction.id, 4);
        //make array
        auction.universes = ("" + auction.universesIWillCreateFor).split(", ");
        auction.universeTags = auction.universes.map(universe => {
            let newU = universe.toLowerCase().replace(/\'/g, "").replace(/[\s\n\r]/g, "-");
            return newU;
        })
        delete auction.universesIWillCreateFor;
        auction.ratings = auction.ratingsIWillCreateFor.split(", ");
        delete auction.ratingsIWillCreateFor;
        delete auction.modNotes;
    });
    creators.forEach(creator => {
        let creatorNew = Object.assign({}, creator);
        const creatorAuctions = auctions.filter(function(work) {
            return work.creatorName.toLowerCase() == creatorNew.creatorName.toLowerCase();
        });

        if (creatorAuctions.length < 1) {
            console.warn(`WARNING: ${creator.creatorName} has no associated auctions.`)
            empties.push(creator.creatorName);
        } else {
            creatorNew.category = 'tumblrpost';
            creatorNew.title = creatorNew.name;
            creatorNew.layout = 'tumblr';

            const yamlString = '---\n\r' + YAML.stringify(creatorNew) + '---\n\r';

            let aucString = creatorAuctions.map(xyv => {
                const universes = xyv.universes.join(", ");
                const ratings = xyv.ratings.join(", ");
                return `*****\n\r\n\r## ${xyv.workType.desc} Auction\n\r\n\r### Creator’s Description\n\r > ${xyv.workDescription} \n\r > Ratings: ${ratings} \n\r > Universes: ${universes} \n\r\n [CLICK HERE TO BID ON THIS WORK](${mainURL}/${creatorNew.creatorTag}/${xyv.idString}/)`;
            }).join('\n\r\n\r');

            var dir = './source/_posts/';

            fs.writeFileSync(dir + creator.creatorTag + '.md', yamlString + aucString);

            console.log(creator.creatorName + " : " + creatorAuctions.length + " auction works");
        }
    });

    auctions.sort((a, b) => {
        return a.timestamp - b.timestamp;
    });

    auctions.forEach((work, index) => {
        let workNew = Object.assign({}, work);
        let desc = workNew.workDescription;
        const len = 140;
        const creator = creators.find(creator => workNew.creatorName.toLowerCase() === creator.creatorName.toLowerCase());
        workNew.snippet = desc.length <= len ? desc : desc.substr(0, len) + '...';
        delete workNew.timestamp
        workNew.id = pad(workNew.id, 4);
        workNew.category = workNew.creatorTag;
        workNew.title = `${workNew.creatorName}'s ${workNew.workType.slug} Auction`;

        const yamlString = '---\n\r' + YAML.stringify(workNew) + '---\n\r' + desc;

        var dir = './source/_posts/';

        fs.writeFileSync(dir + workNew.id + '.md', yamlString);
    });
}


//Kick off the fetch
request({
    followAllRedirects: true,
    method: 'GET',
    url: "https://script.google.com/macros/s/AKfycbzU-t3AOSIxqQVgDYbK2X8fRqYiUmbEufwsrWG7rYQBo6WcByc/exec"
}, function(error, response, body) {
    if (!error && response.statusCode == 200) return exnihilo(body)
    console.log(error);
});
