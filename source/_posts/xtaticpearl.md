---
timestamp: '2017-05-12T22:53:43.503Z'
creatorName: XtaticPearl
charityName1: 'Planned Parenthood '
charityLink1: 'https://www.plannedparenthood.org/'
charityName2: 'Room To Read'
charityLink2: 'https://www.roomtoread.org/'
charityName3: 'Stand With Standing Rock'
charityLink3: 'http://standwithstandingrock.net/'
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160771566496/xtaticpearl-2017'
allCharities: true
email: mitra.writer@gmail.com
doWant: 'AUs, Happy Endings, angst with happy endings, fluff, team dynamics, all resolved tensions '
doNotWant: 'Non con, hate sex, cheating, abusive relationship, bdsm, hard smut'
universes: null
link1: 'http://archiveofourown.org/users/XtaticPearl/works'
link2: null
link3: null
hasCustomCharities: true
creatorTag: xtaticpearl
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > Word length: 20k+, Completion Time: By July 5 
 > Ratings: G, Teen, Mature 
 > Universes: MCU, Earth's Mightiest Heroes, Avengers Assemble, Avengers Academy, Marvel Adventures, 3490, Other-Fandom-Fusion 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/xtaticpearl/0083/)

*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > Word length: 10k+, Completion Time: By July 5 
 > Ratings: Mature 
 > Universes: MCU, Other-Fandom-Fusion 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/xtaticpearl/0084/)