---
timestamp: '2017-04-30T04:35:10.094Z'
creatorName: Kiyaar
charityName1: 'Planned Parenthood'
charityLink1: 'https://secure.ppaction.org/site/Donation2?df_id=12913&12913.donation=form1&s_src=Evergreen_c3_PPNonDirected_banner&_ga=1.149910004.355611479.1491684069'
charityName2: 'Rape, Abuse & Incest National Network'
charityLink2: 'https://donate.rainn.org/'
charityName3: 'American Civil Liberties Union'
charityLink3: 'https://action.aclu.org/secure/donate-to-aclu'
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160695449337/kiyaar-2017'
modNotes: 'URL updated'
allCharities: true
email: rae.simm@gmail.com
doWant: 'established relationships, 616, angst, porn (check with me about specific kinks beforehand please), noncon/rape, torture, darkfic, crossover universe (again, check, but I can do most comic-based universes), break-ups, unhappy endings, character death, silver fox tony, anger management issue steve. If it''s not listed, ask!'
doNotWant: 'gen, exclusively fluff, getting together (maybe negotiable), kidfic, superfamily, MCU, underage, college/highschool au, genderswap, mpreg, a/b/o, non-powered AUs, scott summers'
universes: null
link1: 'http://archiveofourown.org/users/Kiyaar/pseuds/Kiyaar'
link2: 'http://kiyaar.tumblr.com/tagged/writing'
link3: null
hasCustomCharities: true
creatorTag: kiyaar
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > I am not your gal for fluff. I am your gal for dark, fucked-up, messy, feelings shit. Please see my Ao3 page if you're in doubt. I am also not your gal for MCU. I will write 616. I could maybe be persuaded to write a fusion involving 616 and any 616 derivative (ults/x-factor/1872/whatever).

I do prefer prompts that give me more room to play but it's your money and I will happily write whatever you want me to write. If you have a very specific scenario or kink you want me to tackle, please DM me first just to make sure I'm game. I am moving in July, so that's going to be a thing and it will take me a few months to complete whatever you want me to write. More words = more time, bid accordingly. 

$5 gets you 1000 words. 
$10 gets you 2500 words.
$15 gets you 3-4K. 
$25 gets you 4-5K. 
$35 gets you 6-7K. 
$45 gets you 7-9K. 
$55 gets you 10-15k. 
$65 and up gets you 15-25k.  

If you bid something really ridiculous, I can absolutely tailor my length to your bid if 25k isn't enough for you, again, it will take time. Also, keep in mind that these are minimum word counts and I tend to run over, to your benefit.  
 > Ratings: Teen, Mature, Explicit 
 > Universes: 616, Other-Fandom-Fusion, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/kiyaar/0026/)