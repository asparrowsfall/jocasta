---
timestamp: '2017-05-13T05:13:33.005Z'
creatorName: malfaisant
charityName1: 'Southern Poverty Law Center'
charityName2: ' Council on American-Islamic Relations'
charityName3: 'International Refugee Assistance Project'
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160795054388/malfaisant-2017'
allCharities: true
email: malfaisance@gmail.com
doWant: 'character studies, angst, 616 (all eras pre-Secret Wars), ultimates, MCU (Cap 3-centric), canon compliant stories or canon divergent AUs'
doNotWant: 'excessive fluff or kink (abo, D/s, etc - porn is ok), setting AUs (coffee shop, college, soul mate etc - I like working in the sandbox canon provides)'
universes: null
link1: 'http://archiveofourown.org/users/malfaisant'
link2: null
link3: null
hasCustomCharities: true
creatorTag: malfaisant
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > works will be in the 2-10k range, depending on the prompt, and will take a couple of months! a flexible schedule would be greatly appreciated. other than that I don't have much else to say aside from I would love to write your request to the letter, but I do recommend taking my strengths as a writer into consideration for optimal results. 
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: 616, MCU, Ultimates 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/malfaisant/0102/)