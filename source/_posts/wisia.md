---
timestamp: '2017-05-04T05:06:50.185Z'
creatorName: wisia
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160724837856/wisia-2017'
allCharities: null
email: wisiaden@gmail.com
doWant: 'MCU, Iron Man Noir, almost anything alternate universe -- particularly if fantasy'
doNotWant: 'No incest, scat/watersports - or just ask if I feel comfortable writing certain subjects. You can contact me at wisiaden@gmail.com '
universes: null
link1: 'http://archiveofourown.org/users/wisia'
link2: 'http://wisiaden.tumblr.com/post/110044901004/steve-rogerstony-stark'
link3: null
hasCustomCharities: false
creatorTag: wisia
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > I will write at least a 5K fic of your choice, to be posted on AO3 by end of July/early August.  
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: MCU, Avengers Academy, Noir, Other-Fandom-Fusion, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/wisia/0049/)