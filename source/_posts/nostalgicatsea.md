---
timestamp: '2017-04-30T06:29:43.071Z'
creatorName: nostalgicatsea
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160684882163/nostalgicatsea-2017'
modNotes: 'URL updated'
allCharities: null
email: nostalgicatsea@gmail.com
doWant: "Writing:\n- Tropes/elements: angst, soulmates, slow burn, mutual or one-sided pining, self-sacrifice, amnesia, time travel, reincarnation, fake relationship, relationship of convenience, hurt/comfort, de-aging, dream world, presumed dead, temporary or permanent death, substance abuse and recovery, Tony’s family issues, dark Steve, pre-serum Steve\n- AUs: canon-divergent (I like sticking to canon as much as possible, though), mafia/gangster, sports, non-powered, high school or college, cyberpunk, etc. (I love AUs!)\n- I tend to focus one specific moment or a series of small moments, feelings, and relationships more than action-packed plots\n\nBeta-ing:\n- Tropes, genres, and elements (in addition to the above): Identity porn, captivity, coma, constructed reality, mind control, truth serum, enemies/friends-to-lovers, atonement, PTSD, horror, supernatural, mystery, fantasy, noir, darkfic, Tony’s heart problems, Steve’s struggle with moving on\n- AUs (in addition to the above): historical, futuristic, modern military, space"
doNotWant: "- I’m up for most things except some extreme kinks, incest, cheating, and mpreg. I don’t have any triggers. If you want me to elaborate on my do-not-wants or have a trope, kink, or plot point that you’re not sure I’ll be okay with, please contact me beforehand.\n- Writing: A/B/O, D/S, complicated plots, crack, 100% pure fluff, kidfic, Steve abusing Tony or vice versa, unbalanced CW plots\n- Betaing: A/B/O, D/S, PWPs, tooth-rotting fluff without plot, unbalanced CW plots. I’m not that fond of coffeeshop, apocalypse, zombie, and vampire AUs, but I can help with them"
universes: null
link1: 'http://archiveofourown.org/users/nostalgicatsea'
link2: 'http://nostalgicatsea.tumblr.com/tagged/my-fics'
link3: null
hasCustomCharities: false
creatorTag: nostalgicatsea
category: tumblrpost
title: null
layout: tumblr
---
*****

## Other fanwork-related services, including Beta-ing, Consulting, etc. Auction

### Creator’s Description
 > I can help with SPaG, pacing, sentence structure, and continuity and particularly love discussing characterization, worldbuilding, plot, and character development. I tend to like plots that tackle serious issues or big canon points, complex plots, and plots with multiple layers/levels. I also love character and relationship studies and contemplative pieces that really dive deep into who the characters are and how they feel.

Notes on universes: For 616 and Ultimates, I’ll only do an AU or a story that does not rely heavily on canon. I’ve only watched a handful of Avengers Assemble episodes, so please keep that in mind if you want me to beta an AA fic.

Turnaround depends on fic length and my schedule. I’m fast, but if something comes up, I’ll let you know immediately.

If you need references, I’ve beta-ed and acted as a sounding board for several writers such as aslightstep, captainshellhead/vibraniumstark, laireshi, Sineala, and Woad. 
 > Ratings: G, Teen, Mature 
 > Universes: 616, MCU, Noir, 3490, Ultimates, Avengers Assemble, 1872, Avengers Academy, Earth's Mightiest Heroes, Other-Fandom-Fusion, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/nostalgicatsea/0010/)

*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > Please keep in mind that I tend to be slow at writing and I write short fics. The fic will probably be 1-2k at most. This is not to say that I won't end up with something longer, but I can't make any promises. Please don't bid on me unless you're okay with that length or a potentially long wait! That said, I'll keep you updated on the fic progress, so you're not in the dark. 

If you want 616, EMH, or AA, contact me beforehand to see if I can do it.  
 > Ratings: G, Teen 
 > Universes: MCU, Avengers Academy, Noir 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/nostalgicatsea/0077/)