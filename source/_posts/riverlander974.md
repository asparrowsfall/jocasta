---
timestamp: '2017-05-13T15:54:56.949Z'
creatorName: Riverlander974
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160812969178/riverlander974-2017'
allCharities: null
email: edismith@hotmail.co.uk
doWant: 'Fluff, angst, hurt/comfort, domestic, family, or any of the usual tropes, I''ll give it a go :)'
doNotWant: 'Highly explicit sexual content (I can to try add some smut or hints to it, but not really my strong suit)'
universes: null
link1: 'https://archiveofourown.org/users/Riverlander974/works'
link2: 'https://riverlander974.tumblr.com/tagged/river-writes'
link3: null
hasCustomCharities: false
creatorTag: riverlander974
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > For every $5 raised, I can write ~2k words for the winner. Prompt(s) up to winner.
(Can be split into several small drabbles, or add words all up into longer fics, max ~7k per fic).

If more than $50 is raised, I can write a multi-chaptered fic, 20k-40k words. (Or you can split this down into multiple one-shots - as above).

For every additional $30 raised after $50, I can write another multi-chaptered fic, ~20k words (or work with the winner on extending a previous prompted fic). 

If more than $100 is raised, I will offer a special look forward into the Carter Cousin Chronicles saga, beyond SNAFU, involving an Infinity Gem. (Winner must NOT pass this preview or any spoilers onto others. This is exclusively for the winner's eyes!)

Complimentary fic banner made my me for every fic I write! :D

If you want a crossover/au, it will depend on the fusion fandom, so please check beforehand please! (Can contact me on my Tumblr). Works may take some time to complete (I'm in the middle of moving) 
 > Ratings: G, Teen, Mature 
 > Universes: MCU, Avengers Assemble, Avengers Academy, Other-Fandom-Fusion 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/riverlander974/0117/)