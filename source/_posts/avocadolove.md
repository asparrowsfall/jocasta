---
timestamp: '2017-05-13T17:02:58.571Z'
creatorName: AvocadoLove
charityName1: 'Sierra Club'
charityLink1: 'http://www.sierraclub.org/'
charityName2: 'Natural Resources Defence Council'
charityLink2: 'https://www.nrdc.org/'
charityName3: 'Yosemite Conservancy'
charityLink3: 'https://www.yosemiteconservancy.org/'
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160846084745/avocadolove-check-out-avocadoloves-existing-works'
allCharities: false
email: voodoo.weasel@gmail.com
doWant: "ONE (1) fanfiction of a length 5,000 words or longer. (Most likely longer.)\n\nI'm happy with these plot elements: Plot, canon divergences, alternate universes (within canon-ish), general 'what if' scenarios, A/B/O, Soulmate,  dub-con, villain Steve Rogers, presumed dead, angst, slow burn, de-aging. Possible crossover if I'm familiar with the other fandom. PLOT.\n\nWinner will also receive an ebook version of this fic (text-based cover unless the winner supplies their own) in epub, mobi, and PDF formats with a dedication thanking the winner for their generous donation."
doNotWant: 'I would absolutely be terrible at writing anything that requires thoughtful depth and sensitivity. (Transgender issues, asexual, rape trauma, character disability). No explicit sex scenes, non-powered/high school/coffee shop AU''s, or super family. '
universes: null
link1: 'http://archiveofourown.org/users/AvocadoLove/pseuds/AvocadoLove'
link2: 'https://www.fanfiction.net/u/857178/AvocadoLove'
link3: null
hasCustomCharities: true
creatorTag: avocadolove
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > 5,000+ minimum word length fic. No maximum. Winner will also receive one formatted ebook copy (with text based cover unless winner provides a picture) of the fic in ePub, Mobi, or PDF file format.  
 > Ratings: G, Teen, Mature 
 > Universes: MCU 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/avocadolove/0124/)