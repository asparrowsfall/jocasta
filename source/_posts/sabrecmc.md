---
timestamp: '2017-04-30T13:16:28.636Z'
creatorName: sabrecmc
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160692478997/sabrecmc-2017'
modNotes: 'URL updated'
allCharities: null
email: sabrecmc@hotmail.com
doWant: 'MCU; AU of pretty much any variety; A/B/O/; anything related to a fic I''ve already written; NSFW is fine if you want it'
doNotWant: 'Poly relationships; BDSM; kid fic; age play; non-con between Steve and Tony'
universes: null
link1: 'http://archiveofourown.org/users/sabrecmc/pseuds/sabrecmc'
link2: null
link3: null
hasCustomCharities: false
creatorTag: sabrecmc
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > I think I can comfortably assure the winner of a fic of no less than 5K, but will try to have the fic a length that reflects the winning bid and the demands of the story, whatever it may be.  I am slow, so you will need to bear with me!  Things do get finished.  This fic will take precedence over others, however.  

I will be happy to write a brand new story based on your idea or something related to one of my existing stories.  You can give me a very basic idea or something with more specifics you'd like to see.  The fic is for you, so totally your call!   
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: MCU, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/sabrecmc/0013/)