---
timestamp: '2017-05-11T04:03:29.641Z'
creatorName: Impala_Chick
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160824779340/impalachick-2017'
allCharities: null
email: kalsgirl@gmail.com
doWant: 'MCU or Avengers Academy ''verse, happy endings, crossovers, kink exploration'
doNotWant: 'A/B/O, Soulmates AU, Library/Coffee-Shop AU, water sports, cross dressing'
universes: null
link1: 'http://archiveofourown.org/users/Impala_Chick/works'
link2: null
link3: null
hasCustomCharities: false
creatorTag: impala-chick
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > I will write 5k -10k total, on a prompt or multiple prompts of your choice! I have a work event this summer, so expect your fic(s) by the end of 2017. AUs, Crossovers, and Fusions are welcome. Feel free to prompt with songs, tags, or paragraphs of ideas! I'm a multi-fandom writer, but I'm most known for writing domestic fic, space fusions, and niche kink requests. My "most famous" Stony fic is this one: http://archiveofourown.org/works/7215034. 
 > Ratings: Mature, Explicit 
 > Universes: MCU, Avengers Academy, Other-Fandom-Fusion, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/impala-chick/0069/)