---
timestamp: '2017-05-13T00:55:52.314Z'
creatorName: aslightstep
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160807205932/aslightstep-2017'
allCharities: true
email: hac544@outlook.com
doWant: 'Prefer allowance for happy ending'
doNotWant: 'D/s, anything super dark'
universes: null
link1: 'http://archiveofourown.org/users/aslightstep/works'
link2: null
link3: null
hasCustomCharities: true
creatorTag: aslightstep
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > Normally write between 5 to 15k wordcount. It might take a while for this work to be completed (within 2-3 months).  
 > Ratings: G, Teen 
 > Universes: MCU, Avengers Academy 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/aslightstep/0086/)