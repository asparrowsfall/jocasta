---
timestamp: '2017-05-12T19:14:18.889Z'
creatorName: Amonae
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160763881099/amonae-2017'
allCharities: null
email: amonaewrites@gmail.com
doWant: 'Multiple AUs, smut, hurt/comfort, angst with a happy ending, identity porn, etc. Basically ANYTHING that''s not in the DNWs. I''m most familiar with MCU, 1872, and SOME 616. Will do art, fiction, graphic edits, MVs. I don''t have time to start on this project until the Fall, and it does take me quite some time to finish things as they tend to get away from me.'
doNotWant: 'Genderswap, mpreg, scat, water sports, crossover fic (using characters from outside the Marvel Universe--using plot elements/settings are fine).'
universes: null
link1: 'http://archiveofourown.org/users/Amonae'
link2: 'http://amonaewrites.tumblr.com/search/amonaearts'
link3: null
hasCustomCharities: false
creatorTag: amonae
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > My works tend to be a minimum of 10k, depending on the idea and how much it grows in my head. I won't be able to start until the Fall and it does take me quite some time to finish things, depending on RL commitments and mental blocks. I am willing to work from an intricate plot or a quick idea and will work in conjunction with the bidder to ensure everyone is happy. :) 
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: MCU, 1872, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/amonae/0071/)

*****

## Art, Illustration or Comic Auction

### Creator’s Description
 > I will draw pretty much anything, though I’m not particularly great with mechanical things (as a warning!). I can work with a longer or shorter idea. I take a REALLY LONG TIME with art and won’t be able to start on it until the Fall. 
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: 616, MCU, 1872, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/amonae/0072/)

*****

## Graphics, Gifsets, Edits, etc. Auction

### Creator’s Description
 > I can do comic or MCU edits. These take me a few weeks, but I may not be able to start until late in the Summer/Early Fall. I can do individual images or banners or gif sets or whatever you would like. :) 
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: 616, MCU, 1872, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/amonae/0073/)

*****

## Video Auction

### Creator’s Description
 > I take a long time to put together a video, depending on length. I use Movie Studio Platinum 13.0 and do a lot of video edits on my own, though some things are currently above my skill level (ie. putting in objects that were not in the video to begin with).  
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: 616, MCU, 1872, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/amonae/0074/)

*****

## Podfic Auction

### Creator’s Description
 > While I do have a nice mic, I also live in a shared space and have pets. SO, this being said, there may be occasional background noises. I will do my absolute best to edit them out. If there’s something particularly off, please tell me and I can re-record. The length of time for this one depends on the length of fic. Again, won’t be able to start until the Fall. 
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: 616, MCU, 1872, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/amonae/0075/)