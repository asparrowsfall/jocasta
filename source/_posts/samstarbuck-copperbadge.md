---
timestamp: '2017-05-13T11:46:27.894Z'
creatorName: 'Sam Starbuck / Copperbadge'
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160822785390/copperbadge-2017'
allCharities: null
email: copperbadge@gmail.com
doWant: 'Pretty nearly anything not listed under "do not want to create". '
doNotWant: 'Noncon, unhappy ending, major character death, certain kinks (negotiable), character bashing'
universes: null
link1: 'http://archiveofourown.org/users/copperbadge/works'
link2: null
link3: null
hasCustomCharities: false
creatorTag: samstarbuck-copperbadge
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > Fanfic written to order, any rating, minimum 1K words.  
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: 616, MCU, Earth's Mightiest Heroes, Avengers Assemble, Ultimates, Marvel Adventures, 3490, Other-Fandom-Fusion, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/samstarbuck-copperbadge/0107/)