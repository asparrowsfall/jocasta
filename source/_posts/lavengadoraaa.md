---
timestamp: '2017-05-13T19:25:33.199Z'
creatorName: lavengadoraaa
charityName1: 'Girls Educational & Monitoring Services (GEMS)'
charityLink1: 'http://www.gems-girls.org/about'
charityName2: 'Mariposas Sin Fronteras (Butterflies without Borders - LGBTQ Immigrants in Detention Centers)'
charityLink2: 'https://mariposassinfronteras.org/about-us/'
charityName3: 'The International Refugee Assistance Project'
charityLink3: 'https://irap.urbanjustice.org/irap-about'
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160802177248/lavengadoraaa-2017'
allCharities: true
email: emaildora811xd@gmail.com
doWant: Divorce
doNotWant: Rape
universes: null
link1: 'http://www.lavengadoraaa.tumblr.com/tagged/dora-writes'
link2: 'http://www.archiveofourown.org/users/Missy_dee811/works'
link3: null
hasCustomCharities: true
creatorTag: lavengadoraaa
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > I can write at the rate of up to 5,000 words a month, it might take me a little longer though. I would prefer to write an original work for this but I'm willing to work on a WIP.

I am most comfortable writing angst and hurt/comfort, be it emotional or physical. The hurt can either be internal (self-harm) or external (torture). I am not the person to come to if you want tooth-rotting fluff, my fluff always has feels. Having written graphic scenes, I will. Hence the mature and explicit warnings.

I am most familiar with 616 (mostly 2004-present). However, I will read the relevant comics, if you choose a time period/event with which I'm not familiar.

I won't write Hydra Steve unless it's a fix-it. (Please talk to me about specifics though.) 
 > Ratings: Mature, Explicit 
 > Universes: 616, Earth's Mightiest Heroes, Avengers Assemble, Ultimates, Noir, Other-Fandom-Fusion 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/lavengadoraaa/0119/)