---
timestamp: '2017-05-06T04:34:18.402Z'
creatorName: Arukou
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160737476382/arukou-2017'
allCharities: null
email: silverlightningbolt@hotmail.com
doWant: 'Hurt/comfort, fluff, domestic, get-together, established relationship, AU (nonpowered, historical, fantasy, sci-fi, gen), adventure, Marvel 616, MCU, AA, EMH, smut, BDSM'
doNotWant: 'Dub-con, Non-con, M-preg, Mind control, Major character death, Dark, Unhappy ending, sex involving bodily wastes, pain play of any kind'
universes: null
link1: 'http://archiveofourown.org/users/Arukou'
link2: null
link3: null
hasCustomCharities: false
creatorTag: arukou
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > My work will be no less than 5,000 words, but I cannot guarantee a maximum length. I am happy to work with a donor's prompt, provided it steers clear of subject matter I won't write. The more specific a prompt can be, the better. I hope you'll consider bidding on my work so that we can Trump hate together. 
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: 616, MCU, Earth's Mightiest Heroes, Avengers Assemble, Avengers Academy 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/arukou/0059/)