---
timestamp: '2017-05-13T04:44:13.689Z'
creatorName: Metaldragonsteelwings
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160842428345/metaldragonsteelwings-2017'
allCharities: null
email: metaldragonsteelwings@gmail.com
doWant: 'Indentity porn/confusion, shmoop, hurt/comfort, kid!fic, time travel, multiverse crossovers'
doNotWant: 'ABO, M-Preg, incest, hydra!Steve or anything featuring secret empire, underage sex'
universes: null
link1: 'https://archiveofourown.org/users/Steel_Wings'
link2: null
link3: null
hasCustomCharities: false
creatorTag: metaldragonsteelwings
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > Offering a fanfiction with a minimum of 5k words. For every $10 the bid increases from the original $10, I will add 1k words to the minimum with a cap of 15k words. I anticipate needing about one month to complete the fic. Prompts, especially specific prompts, strongly preferred but not required. 
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: 616, Avengers Assemble, Noir, 3490, Other-Fandom-Fusion, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/metaldragonsteelwings/0113/)