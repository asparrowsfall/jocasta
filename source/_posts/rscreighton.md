---
timestamp: '2017-05-08T03:14:47.590Z'
creatorName: RsCreighton
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160828669345/rscreighton-2017'
allCharities: null
email: rscreighton@live.com
doWant: 'Tropes, Happy Endings'
doNotWant: 'Unhappy Endings, '
universes: null
link1: 'http://archiveofourown.org/users/RsCreighton'
link2: null
link3: null
hasCustomCharities: false
creatorTag: rscreighton
category: tumblrpost
title: null
layout: tumblr
---
*****

## Podfic Auction

### Creator’s Description
 > Other- I'm pretty easy verse-wise for Stony. I will record any length fic, but please aim your bets accordingly (think of it about 10k per $15.) <3 All examples of my works can be found on Ao3 under RsCreighton! <3 Note: Remember that you will need to obtain author permission on the fic (or the author has to have blanket permission) in order for me to start any project, and I do reserve the right to ask you to choose a different fic should the contents make me uncomfortable, (tho really so long as the fic doesn't have an unhappy ending I'm usually pretty easy). :D 
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: 616, MCU, Avengers Assemble, Avengers Academy, Other-Fandom-Fusion, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/rscreighton/0126/)