---
timestamp: '2017-04-30T19:22:14.312Z'
creatorName: grue
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160688033237/grue-2017'
modNotes: 'URL updated'
allCharities: null
email: bozaloshtsh@gmail.com
doWant: 'Humor, worldbuilding/plot, opportunities for bickering because bickering is GREAT.'
doNotWant: 'Infidelity, non-con, explicit sex (takes me too long to write, soz), kidfic.'
universes: null
link1: 'http://archiveofourown.org/users/grue'
link2: null
link3: null
hasCustomCharities: false
creatorTag: grue
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > Story of 5k minimum length. Once I have your Want List at hand it'll take me between 6 and 8 weeks to write it because I am a procrastinator. 
 > Ratings: G, Teen, Mature 
 > Universes: 616, MCU 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/grue/0021/)

*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > 20k minimum-length story. This is for if you want some major worldbuilding or casefic or something that isn't just bickering at eachother while the battle rages. Once I have your Want List (and any ideas you may have for a story you want to read, we can hash that out if you're interested) it should take me twelve weeks maximum to write a full story. 
 > Ratings: Teen, Mature 
 > Universes: 616, MCU 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/grue/0022/)