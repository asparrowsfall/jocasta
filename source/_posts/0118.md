---
id: '0118'
creatorName: Kayvsworld
workType:
    desc: 'Art, Illustration or Comic'
    header_name: Art
    slug: art
workDescription: "$15 - $25: black and white single-character sketch\n$30 - $35: single-character sketch with flat monochromatic colours \n$40+: single-character sketch with 2 flat colours + tones \n\nA single character sketch of Steve or Tony! I’ll start your drawing as soon as possible, but I might not be able to get it finished until a few months from now, because I also have a few commissions I’m already working on! <3"
minimumBid: 15
bidIncrement: 5
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160781546414/kayvsworld-2017'
creatorTag: kayvsworld
idString: '0118'
universes:
    - '616'
    - MCU
    - 'Earth''s Mightiest Heroes'
    - 'Avengers Assemble'
    - 'Avengers Academy'
    - Ultimates
    - 'Marvel Adventures'
    - Noir
    - '1872'
    - '3490'
    - Other-Fandom-Fusion
    - Other
universeTags:
    - '616'
    - mcu
    - earths-mightiest-heroes
    - avengers-assemble
    - avengers-academy
    - ultimates
    - marvel-adventures
    - noir
    - '1872'
    - '3490'
    - other-fandom-fusion
    - other
ratings:
    - G
    - Teen
    - Mature
snippet: "$15 - $25: black and white single-character sketch\n$30 - $35: single-character sketch with flat monochromatic colours \n$40+: single-characte..."
category: kayvsworld
title: 'Kayvsworld''s art Auction'
---
$15 - $25: black and white single-character sketch
$30 - $35: single-character sketch with flat monochromatic colours 
$40+: single-character sketch with 2 flat colours + tones 

A single character sketch of Steve or Tony! I’ll start your drawing as soon as possible, but I might not be able to get it finished until a few months from now, because I also have a few commissions I’m already working on! <3