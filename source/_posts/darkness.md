---
timestamp: '2017-04-30T19:11:43.648Z'
creatorName: darkness
charityName1: 'Liberty in North Korea'
charityLink1: 'http://www.libertyinnorthkorea.org/'
charityName2: 'Tzu Chi'
charityLink2: 'https://www.tzuchi.us/'
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160691215698/darkness-2017'
modNotes: 'URL updated'
allCharities: true
email: whenfireanddarknesscollide@gmail.com
doWant: 'For dark/angst, mainly major character death. I can do fluff, but I do write better with a plot. For calligraphy, I can write anything.'
doNotWant: 'Anything explicit/NSFW. Profanity is fine.'
universes: null
link1: 'https://archiveofourown.org/works/8449228'
link2: 'https://www.fanfiction.net/u/6559386/'
link3: null
hasCustomCharities: true
creatorTag: darkness
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > I will write up to 5,000 words. 
 > Ratings: G, Teen, Mature 
 > Universes: MCU 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/darkness/0019/)

*****

## Art, Illustration or Comic Auction

### Creator’s Description
 > I can write any quote from any universe.  
 > Ratings: G, Teen, Mature 
 > Universes: 616, MCU, Earth's Mightiest Heroes, Avengers Assemble, Avengers Academy, Ultimates, Marvel Adventures, Noir, 1872, 3490, Other-Fandom-Fusion 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/darkness/0020/)