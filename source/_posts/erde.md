---
timestamp: '2017-05-01T23:39:16.355Z'
creatorName: erde
charityName1: ACLU
charityLink1: 'https://www.aclu.org'
charityName2: 'NAACP Legal Defense and Educational Fund'
charityLink2: 'http://www.naacpldf.org'
charityName3: 'Planned Parenthood'
charityLink3: 'https://www.plannedparenthood.org'
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160709534728/erde-2017'
allCharities: true
email: erdesque@gmail.com
doWant: 'I''m game for both fluff and angst.'
doNotWant: 'Other romantic relationships and taking digs at any of the characters.'
universes: null
link1: 'https://erdesque.tumblr.com/tagged/mine'
link2: null
link3: null
hasCustomCharities: true
creatorTag: erde
category: tumblrpost
title: null
layout: tumblr
---
*****

## Graphics, Gifsets, Edits, etc. Auction

### Creator’s Description
 > A gifset or edit revolving around a prompt of your choice. 
 > Ratings: G, Teen, Mature 
 > Universes: 616, MCU, Avengers Assemble, Ultimates, 1872 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/erde/0034/)