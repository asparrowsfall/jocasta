---
timestamp: '2017-04-30T14:36:47.667Z'
creatorName: Karadin
charityName1: 'The Southern Poverty Law Center'
charityLink1: 'https://www.splcenter.org/'
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160757850569/karadin-2017'
allCharities: true
email: karadin@karadin.com
doWant: nudity
doNotWant: 'abuse of any sort'
universes: null
link1: 'https://society6.com/karadin'
link2: null
link3: null
hasCustomCharities: true
creatorTag: karadin
category: tumblrpost
title: null
layout: tumblr
---
*****

## Crafts & Fanmade Merchandise Auction

### Creator’s Description
 > Steve Rogers Pinup Bodypillowcase
artwork by Karadin 
image printed on both sides*
Steve is dressed in a star spangled union suit on one side 
and nude on the other
100% washable polyester with satin feel
vivid colors
finished seams
hidden zipper closure
fits 20 x 54 US size pillow - not included

https://www.etsy.com/listing/468489140/steve-rogers-pinup-bodypillowcase?ref=shop_home_active_3 
 > Ratings: Mature 
 > Universes: MCU 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/karadin/0015/)