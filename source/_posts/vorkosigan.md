---
timestamp: '2017-05-05T14:14:39.645Z'
creatorName: vorkosigan
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160745795207/vorkosigan-2017'
allCharities: null
email: you.walking.carpet@gmail.com
doWant: 'getting together, mutual pining, fluff and angst, angst with a happy ending, hurt/comfort, post cw fix-its, romance, any good old tropes like truth serum or huddling for warmth,  pwp, porn with feels, UST, enemies to friends/lovers, crack, crack treated seriously, any AU''s.'
doNotWant: 'established relationship, character death, cheating, break-ups, abuse, torture, nothing too dark or extreme or overly kinky, D/s verse, A/B/O, unhappy ending for stony, glaringly one-sided things regarding CW'
universes: null
link1: 'http://archiveofourown.org/users/vorkosigan/works'
link2: null
link3: null
hasCustomCharities: false
creatorTag: vorkosigan
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > My schedule can be a little tight, so I don't promise to be super fast, but I can most probably be done in two or three months tops (maybe sooner). I'll do regular prose or any epistolary/texting format. I can't really do poetry, unless it's of the narrative variant, as in Anglo-Saxon narrative poems (done in modern English), which... I don't think anyone really wants. I can perhaps write a play, if someone is really into that? I won't do drabbles or super short things, because I don't know how to. Let's say something between... 3k and 10k words would be cool (but I need this to be flexible, because I can be a little long winded - as is probably obvious). I mostly write post-CACW stories, but in such case I'd rather not get too political and would focus on the feels (because... opinions differ). Besides from the verses I've ticked here, I'll also write any AU (except for D/s and A/B/O - really, any, as long as I know anything about it at all) - like medieval or steampunk or music/theatre AU or whatever, or something based on your favorite film (something I can watch in under 3 hours, so no TV shows unless I've seen them already). Also, English is not my first language, but if you're skeptical, feel free to look through my works on AO3 (i.e. it will be okay :) ). 
 > Ratings: Teen, Mature, Explicit 
 > Universes: MCU, Noir, 1872 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/vorkosigan/0052/)