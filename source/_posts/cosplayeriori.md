---
timestamp: '2017-05-05T22:40:18.657Z'
creatorName: cosplayeriori
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160852068734/cosplayeriori-2017'
allCharities: true
email: cosplayeriori@yahoo.com
doWant: 'fluff, smut, happy endings, fixing it'
doNotWant: 'extreme angst, character death '
universes: null
link1: 'http://archiveofourown.org/users/wannabehokage/works'
link2: null
link3: null
hasCustomCharities: true
creatorTag: cosplayeriori
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > You will get at LEAST 2K of story written for you and a single prompt or single sentence start from the winner. 

For every $15 dollar increments bid an extra prompt or extra sentence of description will be given to the winner.  So if you have a lot of prompt elements, you’d like or a paragraph of description then make sure you bid! The more prompt and description you’ll also get MORE words! What a deal! 

Do some good just like the boys we adore and get a story written just for you.  
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: MCU, Avengers Assemble, Avengers Academy, 1872 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/cosplayeriori/0057/)