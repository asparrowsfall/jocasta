---
timestamp: '2017-05-04T08:14:41.293Z'
creatorName: eustassya
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160726905282/eustassya-2017'
allCharities: null
email: dovermun@gmail.com
doWant: 'identity porn, angst, pining, Tony POV, insecurities'
doNotWant: 'actual porn, A/B/O, non-con, permanent major character death, polyamory, gore, genderswap, extremely long and complicated plots'
universes: null
link1: 'https://archiveofourown.org/users/eustassya'
link2: 'https://tonium.tumblr.com/tagged/original'
link3: null
hasCustomCharities: false
creatorTag: eustassya
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > less than 5000 words, may take a few weeks/months to complete due to final year exams  
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: 616, Avengers Assemble, Avengers Academy, 1872 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/eustassya/0050/)

*****

## Graphics, Gifsets, Edits, etc. Auction

### Creator’s Description
 > still images only, i.e. no gifsets and video edits. may take up to a few weeks depending on request. maximum 9 edits in 1 set 
 > Ratings: G, Teen, Mature 
 > Universes: 616, Avengers Assemble, 1872 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/eustassya/0051/)