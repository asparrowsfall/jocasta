---
timestamp: '2017-05-01T18:57:39.275Z'
creatorName: Veldeia
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160707319552/veldeia-2017'
allCharities: null
email: veldeia@yahoo.com
doWant: 'Hurt/comfort / whump is the genre I most enjoy writing (and think I''m best at), but I''ll happily write almost anything!'
doNotWant: 'I will not write kidfic/family themed fic, non-con, or permanent character death.'
universes: null
link1: 'http://archiveofourown.org/users/Veldeia/pseuds/Veldeia/works'
link2: null
link3: null
hasCustomCharities: false
creatorTag: veldeia
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > A fic of 4-5k words written just for you!

My specialties include hurt/comfort (particularly hurt!Tony, but I'll also happily write hurt!Steve if you prefer that), wilderness survival settings (particularly caves and mountains), dinosaurs, and molecular biology, to name a few.

I'm open to writing pretty much anything not ruled out by my 'verse, rating and "will not create" limits, but if you'd like to discuss your idea with me before bidding, you can contact me on tumblr, @veldeia. 
 > Ratings: G, Teen 
 > Universes: MCU, Avengers Assemble, Noir 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/veldeia/0032/)