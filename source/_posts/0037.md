---
id: '0037'
creatorName: avengercat
workType:
    desc: 'Fiction, Prose, or Other Writing'
    header_name: Fic
    slug: fic
workDescription: "That PWP you've always been searching for. Or another short-to-midlength joy-inducing fanfic. \n\nI'll begin writing in mid-June and will aim to have it completed by the end of summer, but it may take until the end of this year as I’m also signing up for the C/IM Big Bang."
minimumBid: 20
bidIncrement: 1
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160721803945/avengercat-2017'
creatorTag: avengercat
idString: '0037'
universes:
    - '616'
    - MCU
    - 'Earth''s Mightiest Heroes'
    - 'Avengers Assemble'
    - 'Avengers Academy'
    - Other-Fandom-Fusion
    - Other
universeTags:
    - '616'
    - mcu
    - earths-mightiest-heroes
    - avengers-assemble
    - avengers-academy
    - other-fandom-fusion
    - other
ratings:
    - Explicit
snippet: "That PWP you've always been searching for. Or another short-to-midlength joy-inducing fanfic. \n\nI'll begin writing in mid-June and will aim ..."
category: avengercat
title: 'avengercat''s fic Auction'
---
That PWP you've always been searching for. Or another short-to-midlength joy-inducing fanfic. 

I'll begin writing in mid-June and will aim to have it completed by the end of summer, but it may take until the end of this year as I’m also signing up for the C/IM Big Bang.