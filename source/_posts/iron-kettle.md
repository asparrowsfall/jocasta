---
timestamp: '2017-05-04T03:09:48.114Z'
creatorName: Iron__Kettle
charityName1: 'American Civil Liberties Union'
charityLink1: 'https://www.aclu.org/'
charityName2: 'Planned Parenthood'
charityLink2: 'https://www.plannedparenthood.org/'
charityName3: 'Sierra Club'
charityLink3: 'http://www.sierraclub.org/'
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160751665697/ironkettle-2017'
allCharities: true
email: camatie@gmail.com
doWant: 'I love cheesy and emotional stuff, let’s be real, and drawing stuff where they’re working together [on or off the field]'
doNotWant: 'furries, REALLY detailed robots/mechs'
universes: null
link1: 'http://whatthepatrick.tumblr.com/tagged/camatie%20art'
link2: 'http://whatthepatrick.tumblr.com/tagged/plushies'
link3: null
hasCustomCharities: true
creatorTag: iron-kettle
category: tumblrpost
title: null
layout: tumblr
---
*****

## Art, Illustration or Comic Auction

### Creator’s Description
 > Work may take a little bit because of work and life stuff, will finish asap 
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: MCU, Avengers Academy, 3490, Other-Fandom-Fusion 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/iron-kettle/0045/)

*****

## Crafts & Fanmade Merchandise Auction

### Creator’s Description
 > Specifically plushies! I can make SteveTony plushies kiss!!! 
 > Ratings: G, Teen 
 > Universes: MCU, Avengers Academy, Other-Fandom-Fusion 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/iron-kettle/0046/)