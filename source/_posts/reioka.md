---
timestamp: '2017-05-13T06:34:28.634Z'
creatorName: reioka
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160800823346/reioka-2017'
allCharities: null
email: reioka009@gmail.com
doWant: 'fluff, hurt/comfort, a/b/o dynamics, I''ll give most subjects a try'
doNotWant: 'Rape, sexual assault'
universes: null
link1: 'http://archiveofourown.org/users/Reioka/works'
link2: 'https://reioka.tumblr.com/tagged/reioka-writes'
link3: null
hasCustomCharities: false
creatorTag: reioka
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > I will write 1K words for every five dollars raised. I currently am unemployed so have plenty of time to work on the piece but if that changes I will let the winner know and we can work out logistics from there. 
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: MCU, Avengers Assemble, Avengers Academy, Other-Fandom-Fusion, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/reioka/0105/)