---
timestamp: '2017-05-07T15:20:50.869Z'
creatorName: asparrowsfall
charityName1: 'Swing Left'
charityLink1: 'https://secure.actblue.com/contribute/page/swingleft'
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160743654673/asparrowsfall-2017'
allCharities: true
email: asparrowsfall@gmail.com
doWant: 'I''m open to most anything subject-wise.'
doNotWant: 'I don''t want to create anything very explicit--suggestive is fine.'
universes: null
link1: 'http://asparrowsfall.tumblr.com/tagged/mine/'
link2: 'http://asparrowsfall.tumblr.com/post/120728626707/i-made-a-thing-mostly-for-myself-thought-id'
link3: null
hasCustomCharities: true
creatorTag: asparrowsfall
category: tumblrpost
title: null
layout: tumblr
---
*****

## Graphics, Gifsets, Edits, etc. Auction

### Creator’s Description
 > Subtype: Graphic. One title graphic or e-book cover for a fic in the style of your choice. Will include fic title, author's name, and graphical elements.

Can be your fic, or someone else's, provided you have the author's permission. I can do photo manips, text and graphic treatments, and if the bids get high enough, I can even throw in some original art. :) It can be a motif from the fic, or a scene, or whatever you like, as long as its not 100% explicit. Let me know if you have questions, thanks! 
 > Ratings: G, Teen, Mature 
 > Universes: 616, MCU, Earth's Mightiest Heroes, Avengers Assemble, Ultimates, Noir, 1872, 3490, Other-Fandom-Fusion, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/asparrowsfall/0061/)

*****

## Art, Illustration or Comic Auction

### Creator’s Description
 > Subtype: Illustration. Original art for a fic! Can be your fic or someone else's, provided you have the author's permission. It will be at least a simple one color sketch of one character; depending on bid amount, it can be more characters or a more complex scene, or have color. Send me your ideas and we'll figure out what it'll look like! :D  
 > Ratings: G, Teen, Mature 
 > Universes: 616, MCU, Avengers Assemble, Ultimates, Noir, 1872, Other-Fandom-Fusion 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/asparrowsfall/0062/)