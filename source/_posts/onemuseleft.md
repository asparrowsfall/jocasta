---
timestamp: '2017-05-12T22:46:41.712Z'
creatorName: Onemuseleft
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160850018091/onemuseleft-2017'
allCharities: null
email: onemuseleft@yahoo.com
doWant: 'domestic fic, team as family, hurt-comfort, first time, established relationship, pretty much anything that''s not on the NOT list below '
doNotWant: 'detailed non-con (off screen or referenced is fine, as long as it''s not between Steve and Tony), scat, mpreg, A/B/O, Superfamily'
universes: null
link1: 'http://archiveofourown.org/users/nightwalker'
link2: 'http://onemuseleft.tumblr.com/search/nightwalker+writes'
link3: null
hasCustomCharities: false
creatorTag: onemuseleft
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > Up to 5000 words of fic based on prompts, outlines or requests provided by the winning bidder. Bidder may have as much or as little input to the content as they like (ie: you can give me a two word prompt and turn me loose, or you can give me a specific outline and include scenes/conversations/images you'd specifically like to see). If the bid goes super high I will probably raise the word count. Bidders are more than welcome to email me or message me on Tumblr to discuss ideas or ask questions. 
 > Ratings: G, Teen, Mature 
 > Universes: 616, MCU, Earth's Mightiest Heroes, Marvel Adventures 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/onemuseleft/0082/)