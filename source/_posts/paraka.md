---
timestamp: '2017-05-13T02:40:54.110Z'
creatorName: paraka
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160838986619/paraka-2017'
allCharities: null
email: parakaproductions@gmail.com
doWant: 'Things that I love (an incomplete list): Rule 63, identity porn, trans characters, world exploration, found family, pretend dating (really, most harlequin-type storylines), angst, AUs, origin stories, power dynamics (including D/s and BDSM), emotional punches in the gut (I seem to cry while recording a lot of my Avengers podfic).'
doNotWant: "I don't have many no go zones but cannibalism and killing people in a fetishized way are really hard stops for me. Related to the previous point, I mostly can't deal with zombies and am finding vampires harder to deal with too. Detailed gore on a large scale also makes me queasy and I'm not a fan of horror. \nI don't enjoy stories whose plots are driven by homophobia, and I've been finding talk of conversion programs triggery lately. \nI'm also kind of hesitant about stores with cheating? If the cheating is actually addressed in the story, I'm generally ok with it, but if it's handwaved or something, that tends to make me sad."
universes: null
link1: 'http://archiveofourown.org/users/paraka/'
link2: null
link3: null
hasCustomCharities: false
creatorTag: paraka
category: tumblrpost
title: null
layout: tumblr
---
*****

## Podfic Auction

### Creator’s Description
 > I will offer a podfic based on a fic up to 20K in length. You can pick the fic (dependant on author approval) or we can find one together. I only checked off the universes I have read in, but if you are willing to find the story in one of the other universes (and understand my voices will more reflect 616/MCU characters) I'm willing to give it a shot. 

As for delivery of the finished product, I will do my best to get it done as quickly as possible, however I am not a fast podficcer. 20K is about 2.5h finished and I'm generally at a 20:1 ratio for work put in for every finished minute. So let's give me 2 months to be safe. 
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: 616, MCU, 3490 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/paraka/0090/)