---
timestamp: '2017-05-13T13:07:29.046Z'
creatorName: ann2who
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160817126686/ann2who-2017'
allCharities: null
email: ann2who@googlemail.com
doWant: 'Happy Ending, Angst and Fluff and potentially Smut, MCU mostly but would do 616 as well'
doNotWant: 'No Major Character Death, No Mpreg, No Unhappy Ending'
universes: null
link1: 'http://archiveofourown.org/users/ann2who/works'
link2: 'http://stark-spangled-lovers.tumblr.com/tagged/annwrites'
link3: null
hasCustomCharities: false
creatorTag: ann2who
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > Whew, okay, I'm doing this! I'm a bit of a slow writer, so it will at least take me a month to get anything done. I would try and aim for a 10k fic, but it's really a matter of what I'm gonna write. I'm absolutely open to prompts and I'll try to meet your expectations as long as it's not an unhappy ending, mpreg or a major character death. Excited! 
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: 616, MCU, Avengers Academy 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/ann2who/0108/)