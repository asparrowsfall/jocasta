---
timestamp: '2017-05-05T02:35:26.506Z'
creatorName: Mushroom
charityName1: 'Medecin Sans Frontiers (MSF) International'
charityLink1: 'http://www.msf.org'
charityName2: 'International Rescue Committee'
charityLink2: 'https://www.rescue.org'
charityName3: 'United Nations High Commissioner for Refugees (UNHCR) '
charityLink3: 'http://www.unhcr.org'
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160834799056/mushroom-2017'
allCharities: false
email: midnight_medley@yahoo.com
doWant: 'Short Comics, graphics, Superfamily, loving avengers marvel family, any verse, multiverse, happy Tony, size diff, bottom tony'
doNotWant: 'Smut (just for this auction :D)'
universes: null
link1: 'http://mushroomhobbit.tumblr.com'
link2: 'http://archiveofourown.org/users/Mushroom'
link3: null
hasCustomCharities: true
creatorTag: mushroom
category: tumblrpost
title: null
layout: tumblr
---
*****

## Art, Illustration or Comic Auction

### Creator’s Description
 > I may create black and white short comics or colored art :) I mostly specialize in funny, happy Avengers Marvel characters and Superfamily who support Stony as you can see in my comics :) I can also draw multiverse SteveTonies in love with each other and AUs. I can do angst but honestly for this auction I want to share something positive so that it can make people hopeful and related to this positive cause :)

If I am uncomfortable with the request please consult with me again so we can work on something else together :) 
 > Ratings: G, Teen, Mature 
 > Universes: 616, MCU, Earth's Mightiest Heroes, Avengers Assemble, Avengers Academy, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/mushroom/0066/)