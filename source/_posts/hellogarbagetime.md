---
timestamp: '2017-04-28T21:28:04.175Z'
creatorName: hellogarbagetime
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160690151546/hellogarbagetime-2017'
modNotes: 'URL updated'
allCharities: null
email: echan.art@gmail.com
doWant: 'I''m okay with drawing (almost) anything! '
doNotWant: 'I won’t draw any explicit content (nudity, sexual content, graphic depictions of violence, etc.); implied content is okay. If I take issue with the subject matter, or if something is outside my comfort zone, I will not draw it, so please check in with me beforehand. I will only draw/include other relationships as how they are canonly depicted in the source material.'
universes: null
link1: 'http://hellogarbagetime.tumblr.com/tagged/hello%20garbage%20tag'
link2: null
link3: null
hasCustomCharities: false
creatorTag: hellogarbagetime
category: tumblrpost
title: null
layout: tumblr
---
*****

## Art, Illustration or Comic Auction

### Creator’s Description
 > A drawing of Steve and/or Tony! You may give me a simple prompt or detailed description, whatever works for you!

OPTION A
$50 or less, black and white drawing with tones or slight coloring.
$55-$100, colored drawing.
$105 or more, full drawing (including background, color, etc.)

OPTION B
One drawing with tones/slight coloring for every $50 raised (rounding up).

(Note: Because of my workload/schedule, it may take me several weeks to get your drawing done.) 
 > Ratings: G, Teen 
 > Universes: 616, MCU, Noir, 3490, Ultimates, Avengers Assemble, 1872, Other-Fandom-Fusion, Avengers Academy, Marvel Adventures, Earth's Mightiest Heroes, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/hellogarbagetime/0001/)

*****

## Art, Illustration or Comic Auction

### Creator’s Description
 > Comic (black and white, with tones or slight coloring) for an idea of your choice! I can work with anything from a simple prompt to a full script. 

The approximate number of panels/pages I’ll draw will be at a rate of one panel for every $10 raised, OR one page for every $50 raised. The total amount of panel/pages can be divided up among multiple ideas/works. (For example, if the winning bid is $100, I’ll draw a 2-page comic, or a 10-panel comic. Or two 1-page comics, or two 5-panel comics.)

(Note: Because of my workload/schedule, it may take me several weeks to get your comic done.) 
 > Ratings: G, Teen 
 > Universes: 616, MCU, Noir, 3490, Ultimates, Avengers Assemble, 1872, Avengers Academy, Marvel Adventures, Earth's Mightiest Heroes, Other-Fandom-Fusion, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/hellogarbagetime/0002/)