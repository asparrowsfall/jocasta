---
timestamp: '2017-05-12T23:56:59.459Z'
creatorName: Lienwyn
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160769783778/lienwyn-2017'
allCharities: null
email: lienwyn@hotmail.com
doWant: 'Fluff, angst, action, AUs'
doNotWant: 'Extreme graphic violence/gore, porn, non-con, mpreg'
universes: null
link1: 'http://lienwyn.tumblr.com/tagged/Stony'
link2: null
link3: null
hasCustomCharities: false
creatorTag: lienwyn
category: tumblrpost
title: null
layout: tumblr
---
*****

## Art, Illustration or Comic Auction

### Creator’s Description
 > A black/white or colour illustration of your choice. The higher the final bid, the more complex the illustration. 
 > Ratings: G, Teen 
 > Universes: 616, MCU, Earth's Mightiest Heroes, Avengers Assemble, Avengers Academy, Noir, 1872, 3490 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/lienwyn/0085/)