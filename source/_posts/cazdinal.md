---
timestamp: '2017-05-01T01:32:34.977Z'
creatorName: Cazdinal
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160700980301/cazdinal-2017'
allCharities: null
email: cazdinal@gmail.com
doWant: 'Able to create basically anything you want - any iteration of Steve/Tony and/or other characters!  Also can draw in a variety of styles so see my portfolio/tumblr for options for particular styles.'
doNotWant: 'Unfortunately, I will not be able to create any NSFW works, sorry!'
universes: null
link1: 'http://cazdraws.tumblr.com/tagged/my+art'
link2: 'http://cazdinal.carbonmade.com'
link3: null
hasCustomCharities: false
creatorTag: cazdinal
category: tumblrpost
title: null
layout: tumblr
---
*****

## Art, Illustration or Comic Auction

### Creator’s Description
 > Can create either one illustration piece OR a short comic (full color w/background) - to see some examples, visit my tumblr at cazdraws.tumblr.com/tagged/my+art. Pretty flexible in terms of style and in terms of deadlines; I can make most things work if you need the work by a certain time! :)   
 > Ratings: G, Teen 
 > Universes: 616, MCU, Earth's Mightiest Heroes, Avengers Assemble, Avengers Academy, Ultimates, Marvel Adventures, Noir, 1872, 3490, Other-Fandom-Fusion 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/cazdinal/0024/)