---
timestamp: '2017-05-05T22:18:32.216Z'
creatorName: izazov
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160741523714/izazov-2017'
allCharities: null
email: malu5@net.hr
doWant: 'all kinds of angst, fluff, humor, h/c, non-explicit smut, alternate universe'
doNotWant: 'explicit smut, abo, mpreg, character bashing'
universes: null
link1: 'https://archiveofourown.org/users/izazov/pseuds/izazov'
link2: 'http://izazov.tumblr.com/tagged/my-fic'
link3: null
hasCustomCharities: false
creatorTag: izazov
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > The fic I am offering to write should be at least 5k words in length. Depending on the subject, it could reach 15k. But, please, keep in mind that I am a slow writer with inconvenient work hours and it could take me a while to finish it. 
 > Ratings: G, Teen, Mature 
 > Universes: MCU 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/izazov/0056/)