---
id: '0108'
creatorName: ann2who
workType:
    desc: 'Fiction, Prose, or Other Writing'
    header_name: Fic
    slug: fic
workDescription: 'Whew, okay, I''m doing this! I''m a bit of a slow writer, so it will at least take me a month to get anything done. I would try and aim for a 10k fic, but it''s really a matter of what I''m gonna write. I''m absolutely open to prompts and I''ll try to meet your expectations as long as it''s not an unhappy ending, mpreg or a major character death. Excited!'
minimumBid: 10
bidIncrement: 5
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160817126686/ann2who-2017'
creatorTag: ann2who
idString: '0108'
universes:
    - '616'
    - MCU
    - 'Avengers Academy'
universeTags:
    - '616'
    - mcu
    - avengers-academy
ratings:
    - G
    - Teen
    - Mature
    - Explicit
snippet: 'Whew, okay, I''m doing this! I''m a bit of a slow writer, so it will at least take me a month to get anything done. I would try and aim for a ...'
category: ann2who
title: 'ann2who''s fic Auction'
---
Whew, okay, I'm doing this! I'm a bit of a slow writer, so it will at least take me a month to get anything done. I would try and aim for a 10k fic, but it's really a matter of what I'm gonna write. I'm absolutely open to prompts and I'll try to meet your expectations as long as it's not an unhappy ending, mpreg or a major character death. Excited!