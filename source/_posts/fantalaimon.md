---
timestamp: '2017-05-13T03:44:47.969Z'
creatorName: fantalaimon
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160765143655/fantalaimon-2017'
allCharities: null
email: fansidhe@outlook.com
doWant: 'fluff, family feels, h/c, generally soft things... trope-y stuff is also welcome!'
doNotWant: 'smut/kink, darkfic, unhappy endings'
universes: null
link1: 'http://archiveofourown.org/works?utf8=%E2%9C%93&work_search%5Bsort_column%5D=revised_at&work_search%5Brelationship_ids%5D%5B%5D=7265&work_search%5Bother_tag_names%5D=&work_search%5Bquery%5D=&work_search%5Blanguage_id%5D=&work_search%5Bcomplete%5D=0&commit=Sort+and+Filter&user_id=fantalaimon'
link2: 'https://fanartstuffs.tumblr.com/'
link3: null
hasCustomCharities: false
creatorTag: fantalaimon
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > I can offer one fic in around the 3-7k range. Exactly how long will depend largely on the prompt I'm given. As a heads up, I can be a bit slow at times, but I will do my best not to keep anyone waiting too long and am happy to provide progress updates if desired! I’m available for questions of any kind at @fanmkii! 
 > Ratings: G, Teen 
 > Universes: 616, MCU, Noir, 1872, 3490 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/fantalaimon/0092/)

*****

## Art, Illustration or Comic Auction

### Creator’s Description
 > I'll do a simple drawing of Steve and Tony (loose lines+basic coloring) as represented in my art examples. If the auction reaches a minimum of $10, the bidder can request up to two additional characters in the drawing, or a second drawing (or panel, if you’d like them to be sequential) of just Steve and Tony. Simple backgrounds and props are okay, but if you’re unsure if what you want is too complex or have any other questions, please feel free to message me @fanmkii. 
 > Ratings: G 
 > Universes: 616, MCU, Avengers Academy, 3490 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/fantalaimon/0096/)