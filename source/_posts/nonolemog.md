---
timestamp: '2017-05-13T07:11:58.930Z'
creatorName: 'Nono le mog'
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160815045825/nono-le-mog-2017'
allCharities: null
email: noem.cartier@free.fr
doWant: 'hurt/comfort, fluff, plot-heavy stuff...'
doNotWant: 'sad endings, major character death, detailed smut, some triggering stuff'
universes: null
link1: 'http://archiveofourown.org/users/NonoLeMog/works'
link2: null
link3: null
hasCustomCharities: false
creatorTag: nonolemog
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > I am offering a story of at least 5000 words (might be more if inspiration strikes, will probably be proportionate to the highest bid), you can give me a trope, a plot bunny, a situation.... and I'll do my best with it! If it gets long, you'll have regular updates and I'll do my best to be done before September. 
 > Ratings: G, Teen, Mature 
 > Universes: MCU, Avengers Assemble, Avengers Academy, Noir, 1872, 3490 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/nonolemog/0106/)