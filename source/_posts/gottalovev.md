---
timestamp: '2017-05-08T00:10:23.188Z'
creatorName: Gottalovev
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160759520377/gottalovev-2017'
allCharities: null
email: gottalovev@yahoo.ca
doWant: "I will discuss the request of the winner to find an idea that will make them happy. :)  \nWhat I like to write: Pining/obliviousness; will write angst but always with a happy ending; Hurt/Comfort; Alternate universes (except high-school or coffee shop) or fusions if I know the canon; Avengers as family; Bucky as Steve's BFF but not his lover/ex; Close Tony-friendship with Pepper and Rhodey; Pretty much any consensual sexy times except from what is in DNW, but also Gen fic or intimacy without sex; Romance: Enemies to friends to lovers; Established Relationship; Fake Marriage/Relationship; First time / getting together; Near-death confessions; tropes"
doNotWant: 'Major character death; abuse or abusing relationship (especially violence to children or partner); stalking; partner betrayal (infidelity); no underage sex unless both partners are late teens; Hydra Cap; character bashing (even minor character, and especially not previous lovers/interests); suicide – suicide ideation; hurt without comfort; homophobia; medical kink; scat; humiliation; not sure if I could make past Steve/Bucky justice;'
universes: null
link1: 'http://archiveofourown.org/users/gottalovev/works'
link2: null
link3: null
hasCustomCharities: false
creatorTag: gottalovev
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > The 10$ minimum bid will give a (minimum!) 1000 words fic tailor-made for the generous winner! Each 1$ after that is an extra 100 words (minimum!). [Hint? I always, always write more than the minimum.] 

The two highest bidders for this auction will receive a fic each.

Head's up: I do have other engagements, and I am a slow writer. But I will without fail inform the winners of my progress until the fics are done  :)    

See preferences before bidding. If you are not sure about an idea you want, I can be reached by email at gottalovev@yahoo.ca for clarifications. 

Thank you for your generosity! 
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: MCU, Noir, Other-Fandom-Fusion 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/gottalovev/0063/)