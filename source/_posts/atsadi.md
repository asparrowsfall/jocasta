---
timestamp: '2017-04-30T22:30:09.104Z'
creatorName: Atsadi
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160713533297/atsadi-2017'
allCharities: null
email: atsadifish@gmail.com
doWant: 'Magical Realism, Soulmates/Soul marks, Identity Porn, Alternate Universe/History, 3490 & Rule 63, Pre-serum Steve, Time Travel, Science/Speculative Fiction, Polyamory, A/B/O, Historical settings, Iron Dad/Dad!Cap'
doNotWant: 'Explicit porn, Full frontal nudity, Non-con, Abusive relationships, Underage, Hardcore kinks, Character bashing, Anti-accountability, Nazi Cap, Loki, Mpreg'
universes: null
link1: 'http://archiveofourown.org/users/Atsadi/works'
link2: 'http://funkyfish1991.deviantart.com/gallery/'
link3: null
hasCustomCharities: false
creatorTag: atsadi
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > 500 words per $10, up to 10K. Time it takes for me to complete depends on how much fic you buy! Examples of my writing are up on my AO3 (Atsadi). If you want something written that isn't on my "will create" list, please feel free to run it by me before bidding. That being said... provided it's not on my "no-no" list I'm probably open to it. 
 > Ratings: G, Teen, Mature 
 > Universes: MCU, Noir, 1872, 3490, Other-Fandom-Fusion 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/atsadi/0025/)

*****

## Art, Illustration or Comic Auction

### Creator’s Description
 > $30 gets inks for one character. First $20 gets flat colors, next $20 is cel shading, next $20 is full painted colors. For each additional character add $20 to your total. If you bid more than $90 we can discuss full-page comics, painted backgrounds, animations, etc. A more realistic style is worth $120+, and you'll probably want to discuss it with me in advance. Ditto for a traditional medium.

Examples of my art are on deviantART—FunkyFish1991 is my (very) old username and contains more artwork, while Atsadifish is my current username with much less art uploaded. If you particularly like the style of any of my old artworks, you can bring it up with me at any point before bidding closes to make sure your bid amount will cover it.

I will happily illustrate fanfics with author's permission. 
 > Ratings: G, Teen, Mature 
 > Universes: 616, MCU, Earth's Mightiest Heroes, Avengers Assemble, Avengers Academy, Ultimates, Noir, 1872, 3490, Other-Fandom-Fusion, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/atsadi/0028/)

*****

## Video Auction

### Creator’s Description
 > $20 for 30 seconds of video, each additional $10 gets you another 30 seconds, up to 5 minutes. General format I'm thinking is music of your choice + MCU clips (this includes all movies and "Agent Carter," but none of the other TV shows.) If you want something else, best to contact me and make sure it's something I think I can actually deliver. Examples of my video editing can be found on my AO3, username Atsadi.

PLEASE NOTE: Any fandom other than MCU will obviously be trickier to get clips for. I'm game to try it but definitely contact me about your idea first! Video length will be limited also. 
 > Ratings: G, Teen 
 > Universes: MCU, Noir, 1872, 3490 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/atsadi/0029/)