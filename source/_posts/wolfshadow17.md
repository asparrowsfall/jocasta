---
timestamp: '2017-05-13T13:47:12.379Z'
creatorName: Wolfshadow17
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160840653069/wolfshadow17-2017'
allCharities: null
email: sparrow317@gmail.com
doWant: 'Practically any trope'
doNotWant: 'Ageplay/infantilism, mpreg, D/S relationship, A/B/O relationship'
universes: null
link1: 'https://archiveofourown.org/works/search?utf8=✓&work_search%5Bquery%5D=&work_search%5Btitle%5D=&work_search%5Bcreator%5D=Wolfshadow17&work_search%5Brevised_at%5D=&work_search%5Bcomplete%5D=0&work_search%5Bsingle_chapter%5D=0&work_search%5Bword_count%5D=&work_search%5Blanguage_id%5D=&work_search%5Bfandom_names%5D=&work_search%5Brating_ids%5D=&work_search%5Bcharacter_names%5D=&work_search%5Brelationship_names%5D=&work_search%5Bfreeform_names%5D=&work_search%5Bhits%5D=&work_search%5Bkudos_count%5D=&work_search%5Bcomments_count%5D=&work_search%5Bbookmarks_count%5D=&work_search%5Bsort_column%5D=&work_search%5Bsort_direction%5D=&commit=Search'
link2: null
link3: null
hasCustomCharities: false
creatorTag: wolfshadow17
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > For 616, MCU or AU: short story whose length will vary according to the bidder's request but shall not exceed 5 chapters. Please check out my current fics to see what style of writing I do.  
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: 616, MCU, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/wolfshadow17/0109/)