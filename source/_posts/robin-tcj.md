---
timestamp: '2017-05-13T16:18:22.053Z'
creatorName: Robin_tCJ
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160779506074/robintcj-2017'
allCharities: null
email: robintcj.fic@gmail.com
doWant: 'romance, fluff, smut'
doNotWant: 'character death, unhappy endings, dark content, noncon'
universes: null
link1: 'http://archiveofourown.org/works?utf8=✓&work_search%5Bsort_column%5D=revised_at&work_search%5Brelationship_ids%5D%5B%5D=7265&work_search%5Bother_tag_names%5D=&work_search%5Bquery%5D=&work_search%5Blanguage_id%5D=&work_search%5Bcomplete%5D=0&commit=Sort+and+Filter&user_id=Robin_tCJ'
link2: null
link3: null
hasCustomCharities: false
creatorTag: robin-tcj
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > If you win the auction, I will write you a fic 1,000 to 15,000 words, which can include some smut. I'm open to writing PWP, if that's what you're looking for. I'm happy to remix a current work if you want, or write a follow-up or sequel. I'll want you to give me 3 prompts to choose from, though you can indicate a preference for a first choice. I'll be participating in the Big Bang in the fall, so I can start your fic right away, but may not finish until later in the year.  
 > Ratings: Mature, Explicit 
 > Universes: MCU 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/robin-tcj/0115/)