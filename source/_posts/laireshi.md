---
timestamp: '2017-05-01T18:26:19.842Z'
creatorName: Laireshi
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160705125208/laireshi-2017'
allCharities: null
email: laire.shi@gmail.com
doWant: 'angst, multiverse and Marvel crossovers, time travel, emotional hurt/comfort or hurt no comfort, pining, Extremis, Tony''s unhealthy coping habits'
doNotWant: 'poly ships (I''m fine with multiple Steves and Tonys though!), body horror, insects, complete AUs, homophobia, terminal diseases, slow/progressing memory/personality loss'
universes: null
link1: 'http://archiveofourown.org/users/laireshi/'
link2: null
link3: null
hasCustomCharities: false
creatorTag: laireshi
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > I will write a minimum of 2k words, aiming towards longer. I'm having finals in June, so I think I'll be able to work on this fic after that. 

I'm good at writing 616, anything from New Avengers vol. 1 (2005) up until the most recent issues! I like all the angsty events (CW, Director Stark, Hickmanvengers, CW II) and can do fix-its or make-it-worses. I can do established relationship, getting together or even breaking up; I prefer angst but I can write some fluff. 

If you want me to write a universe I haven't picked, I could try to write 1872, Ultimates, or MCU, however, I don't want to promise it - please contact me beforehand if you're interested in it. I can do 616/other Marvel crossovers for most canons, ask me if you have a specific idea. 

I'm better at less detailed prompts, and I'd love it if you could prepare two prompts for me - that said, of course I'll do my best to write whatever you want!

I won't write explicit sex scenes, but I can do suggestive or very dark themes, which is why you can see my chosen ratings going up to Mature. 

If you have any questions, you can contact me on my tumblr @ laireshi.  
 > Ratings: G, Teen, Mature 
 > Universes: 616 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/laireshi/0031/)