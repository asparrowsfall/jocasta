---
timestamp: '2017-04-30T04:32:39.515Z'
creatorName: WreckItRogers
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160739385168/wreckitrogers-2017'
allCharities: null
email: smilies.veronica@gmail.com
doWant: 'Kissing, hugging, holding hands, happiness, cuddling, domestic scenes, multiple characters'
doNotWant: 'NSFW, animals, complex backgrounds'
universes: null
link1: 'http://wreck-it-rogers.tumblr.com/tagged/my%20art'
link2: 'http://wreck-it-rogers.tumblr.com/post/100799702393/nothing-lasts-forever-but-we-will-finished'
link3: null
hasCustomCharities: false
creatorTag: wreckitrogers
category: tumblrpost
title: null
layout: tumblr
---
*****

## Art, Illustration or Comic Auction

### Creator’s Description
 > I will be offering a commission, here is a portfolio of my work: http://wreck-it-rogers.tumblr.com/tagged/my%20art. I work mornings so for whoever wins my auction, I can work on their pieces after work in the afternoons! I can do digital work, but most of my best work comes from pencil and pen works. I am willing to send those pieces to the bidder if they prefer me to do pencil and pen and not digital work! It takes me a range from 1 hour to 9 hours non stop to finish a piece depending on how complex it is.  
 > Ratings: G, Teen 
 > Universes: 616, MCU, Avengers Assemble, Avengers Academy, Earth's Mightiest Heroes, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/wreckitrogers/0005/)

*****

## Art, Illustration or Comic Auction

### Creator’s Description
 > I am offering a finished piece! It took me 5 hours of continuous work. It is a digital piece and I have it ready for the winning bidder! 
 > Ratings: G, Teen 
 > Universes: 616, MCU, Avengers Assemble, Other-Fandom-Fusion, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/wreckitrogers/0006/)