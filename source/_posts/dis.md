---
timestamp: '2017-04-30T14:32:03.826Z'
creatorName: Dis
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160711563873/dis-2017'
allCharities: null
email: danielle.m.romero@gmail.com
doWant: 'I''m open and happy to creating illustration or comics!'
doNotWant: n/a
universes: null
link1: 'http://www.ohhhdis.com'
link2: 'http://www.ohhhdis.deviantart.com'
link3: null
hasCustomCharities: false
creatorTag: dis
category: tumblrpost
title: null
layout: tumblr
---
*****

## Art, Illustration or Comic Auction

### Creator’s Description
 > A custom Stony commission for the winner! Can be fluffy OR saucy ;) 
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: 616, MCU 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/dis/0040/)