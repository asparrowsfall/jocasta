---
timestamp: '2017-05-13T04:51:30.042Z'
creatorName: MusicalLuna
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160789339668/musicalluna-2017'
allCharities: null
email: musicallucy@gmail.com
doWant: 'fluff, friendship, hurt/comfort, kissing, romance, asexuality, emotional hurt/comfort, happy endings, humor, queerplatonic relationships, comfort, cuddling, pining, artist steve rogers, works that are not mcu phase 2 compliant, mcu, mental illness (most familiar with depression, anxiety, ptsd), past peggy/steve, past tony/pepper, steve/pepper/tony, steve/rhodey/tony, willing to try steve/tony/other avenger from pre-aou, mission fic, get together, established relationship'
doNotWant: 'non-con, incest, explicit nsfw content, unhappy endings, fights that don''t get resolved, marvel phase 2 compliance, darkfic, heavy angst, historical-type aus (basically nothing that isn''t set in the modern day or a little bit into the future), character death, infidelity, underage, unrequited love, would prefer not to do steve/tony/bucky but would'
universes: null
link1: 'http://www.archiveofourown.org/users/MusicalLuna'
link2: 'https://www.musicalluna-draws.tumblr.com'
link3: null
hasCustomCharities: false
creatorTag: musicalluna
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > One piece of custom fiction + 1 revision at .012 per word, expect 1-2 weeks per 1000 words for turnaround. over 5k may take considerably longer and will need to come with a detailed fic concept. also willing to do multiple short works up to 10k. 
 > Ratings: G, Teen, Mature 
 > Universes: MCU 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/musicalluna/0097/)

*****

## Art, Illustration or Comic Auction

### Creator’s Description
 > Two character, full body, color digital drawing. Expect at least 2 months for turnaround, particularly if my arm is acting up. 
 > Ratings: G, Teen, Mature 
 > Universes: MCU, Other-Fandom-Fusion 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/musicalluna/0098/)