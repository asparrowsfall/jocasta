---
timestamp: '2017-05-12T18:46:59.543Z'
creatorName: 'Captainshellhead & Vibraniumstark'
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160803726684/captainshellhead-vibraniumstark-2017'
allCharities: null
email: barnesjesse@hotmail.com
doWant: 'Canon-compliant, comics inspired stories. Particularly fond of Silver Age comics-inspired stories. We''re very open, if you have something you''re not sure about just ask!'
doNotWant: 'A/B/O, underage, incest, mpreg, alternate gender'
universes: null
link1: 'http://archiveofourown.org/users/captainshellhead/works'
link2: null
link3: null
hasCustomCharities: false
creatorTag: captainshellhead-vibraniumstark
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > We are offering a 10,000 word fic. 

We are open to writing in most comics universes, but 616 is our baby. AUs are fine, but shoot us a message first to confirm it's not too far out of our comfort zone! Please feel free to contact us before bidding to run your prompt/ideas by us if you have questions. While fics based on recent comics are not off-limits, we aren't fully up-to-date on our reading so keep that in mind when prompting! 
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: 616, Earth's Mightiest Heroes, Ultimates, Marvel Adventures, Noir, 1872 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/captainshellhead-vibraniumstark/0070/)