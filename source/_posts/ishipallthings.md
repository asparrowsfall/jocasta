---
timestamp: '2017-04-28T22:22:47.919Z'
creatorName: ishipallthings
charityName1: 'Planned Parenthood'
charityLink1: 'https://secure.ppaction.org/site/Donation2?df_id=12913&12913.donation=form1&s_src=Evergreen_c3_PPNonDirected_banner&_ga=1.149910004.355611479.1491684069'
charityName2: 'American Civil Liberties Union'
charityLink2: 'https://action.aclu.org/secure/donate-to-aclu'
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160732115126/ishipallthings-2017'
allCharities: true
email: ishipallthings@gmail.com
doWant: 'Fluff, Angst, Canon-divergent, AUs, Friends to Lovers/Enemies to Lovers, Mutual Pining, Action/Adventure, Established Relationship, Kidfic'
doNotWant: 'Major Character Death, Body Horror, Infidelity, Divorce, Not-A-Fix-It, Unhappy Ending, Incest, Rape, Non/Dub-con, Torture, Ageplay, A/B/O'
universes: null
link1: null
link2: null
link3: null
hasCustomCharities: true
creatorTag: ishipallthings
category: tumblrpost
title: null
layout: tumblr
---
*****

## Other fanwork-related services, including Beta-ing, Consulting, etc. Auction

### Creator’s Description
 > Will beta fic ranging from 5-40k. $2 per 1000 words for anything beyond 5k,. Turnaround time depends on length and fic details. My beta services will include cheer-reading and brainstorming help if needed, as well as leaving extended feedback. Please specify if you need me to focus on certain aspects in particular, but I’m happy to just look over characterization, plot/pacing and dialogue overall as well, along with spelling and grammar. I will be making my comments as suggestions, so no pressure there :) If you need references, message me on tumblr! 
 > Ratings: G, Teen, Mature 
 > Universes: MCU, 3490, Avengers Assemble, Marvel Adventures, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/ishipallthings/0003/)