---
id: '0065'
creatorName: Miniraven
workType:
    desc: 'Fiction, Prose, or Other Writing'
    header_name: Fic
    slug: fic
workDescription: 'Willing to write a 5k word fic at max. I''m a very busy person, but I hope to finish within a month of starting the assignment. I''m also willing to negotiate tropes or kinks included in the fic on a case by case basis. '
minimumBid: 5
bidIncrement: 1
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160761901944/miniraven-2017'
creatorTag: miniraven
idString: '0065'
universes:
    - MCU
    - 'Earth''s Mightiest Heroes'
    - 'Avengers Assemble'
    - 'Avengers Academy'
    - Other-Fandom-Fusion
universeTags:
    - mcu
    - earths-mightiest-heroes
    - avengers-assemble
    - avengers-academy
    - other-fandom-fusion
ratings:
    - G
    - Teen
    - Mature
    - Explicit
snippet: 'Willing to write a 5k word fic at max. I''m a very busy person, but I hope to finish within a month of starting the assignment. I''m also will...'
category: miniraven
title: 'Miniraven''s fic Auction'
---
Willing to write a 5k word fic at max. I'm a very busy person, but I hope to finish within a month of starting the assignment. I'm also willing to negotiate tropes or kinks included in the fic on a case by case basis. 