---
id: '0064'
creatorName: Mid
workType:
    desc: 'Art, Illustration or Comic'
    header_name: Art
    slug: art
workDescription: 'traditional sketch, inked, shaded, and scanned -- i can also do au''s and all that fun stuff as long as i have references to work off of'
minimumBid: 15
bidIncrement: 5
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160762852860/mid-2017'
creatorTag: mid
idString: '0064'
universes:
    - '616'
    - MCU
    - 'Earth''s Mightiest Heroes'
    - 'Avengers Assemble'
    - 'Avengers Academy'
    - Ultimates
    - 'Marvel Adventures'
    - Noir
    - '1872'
    - '3490'
    - Other-Fandom-Fusion
    - Other
universeTags:
    - '616'
    - mcu
    - earths-mightiest-heroes
    - avengers-assemble
    - avengers-academy
    - ultimates
    - marvel-adventures
    - noir
    - '1872'
    - '3490'
    - other-fandom-fusion
    - other
ratings:
    - G
    - Teen
snippet: 'traditional sketch, inked, shaded, and scanned -- i can also do au''s and all that fun stuff as long as i have references to work off of'
category: mid
title: 'Mid''s art Auction'
---
traditional sketch, inked, shaded, and scanned -- i can also do au's and all that fun stuff as long as i have references to work off of