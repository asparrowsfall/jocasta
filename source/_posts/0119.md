---
id: '0119'
creatorName: lavengadoraaa
workType:
    desc: 'Fiction, Prose, or Other Writing'
    header_name: Fic
    slug: fic
workDescription: "I can write at the rate of up to 5,000 words a month, it might take me a little longer though. I would prefer to write an original work for this but I'm willing to work on a WIP.\n\nI am most comfortable writing angst and hurt/comfort, be it emotional or physical. The hurt can either be internal (self-harm) or external (torture). I am not the person to come to if you want tooth-rotting fluff, my fluff always has feels. Having written graphic scenes, I will. Hence the mature and explicit warnings.\n\nI am most familiar with 616 (mostly 2004-present). However, I will read the relevant comics, if you choose a time period/event with which I'm not familiar.\n\nI won't write Hydra Steve unless it's a fix-it. (Please talk to me about specifics though.)"
minimumBid: 5
bidIncrement: 5
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160802177248/lavengadoraaa-2017'
creatorTag: lavengadoraaa
idString: '0119'
universes:
    - '616'
    - 'Earth''s Mightiest Heroes'
    - 'Avengers Assemble'
    - Ultimates
    - Noir
    - Other-Fandom-Fusion
universeTags:
    - '616'
    - earths-mightiest-heroes
    - avengers-assemble
    - ultimates
    - noir
    - other-fandom-fusion
ratings:
    - Mature
    - Explicit
snippet: 'I can write at the rate of up to 5,000 words a month, it might take me a little longer though. I would prefer to write an original work for ...'
category: lavengadoraaa
title: 'lavengadoraaa''s fic Auction'
---
I can write at the rate of up to 5,000 words a month, it might take me a little longer though. I would prefer to write an original work for this but I'm willing to work on a WIP.

I am most comfortable writing angst and hurt/comfort, be it emotional or physical. The hurt can either be internal (self-harm) or external (torture). I am not the person to come to if you want tooth-rotting fluff, my fluff always has feels. Having written graphic scenes, I will. Hence the mature and explicit warnings.

I am most familiar with 616 (mostly 2004-present). However, I will read the relevant comics, if you choose a time period/event with which I'm not familiar.

I won't write Hydra Steve unless it's a fix-it. (Please talk to me about specifics though.)