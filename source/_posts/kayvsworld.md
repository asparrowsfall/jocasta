---
timestamp: '2017-05-13T02:20:38.729Z'
creatorName: Kayvsworld
charityName1: 'NAACP Legal Defense and Educational Fund'
charityLink1: 'http://www.naacpldf.org/'
charityName2: 'The Young Center for Immigrant Children’s Rights'
charityLink2: 'http://theyoungcenter.org/'
charityName3: 'The Trevor Project'
charityLink3: 'http://www.thetrevorproject.org/'
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160781546414/kayvsworld-2017'
allCharities: true
email: kayallanart@gmail.com
doWant: 'Fluff! Cuddling! AUs! Angst!'
doNotWant: 'Gore, NSFW stuff'
universes: null
link1: 'http://kayvsworld.tumblr.com/tagged/mine'
link2: 'http://www.kayallanart.com/'
link3: null
hasCustomCharities: true
creatorTag: kayvsworld
category: tumblrpost
title: null
layout: tumblr
---
*****

## Art, Illustration or Comic Auction

### Creator’s Description
 > $25 - $30: Rough sketch (black and white)
$35 - $40: More refined sketch (flat monochromatic colours)
$45 - $50: Inked drawing (black and white)
$55 - $60: Inked drawing (flat colours)
$65-70: Inked drawing with more detail (full colours with texture! a coloured/gradient background can be added!) 
$75+: Add another simple sketch! 

I'll start your drawing as soon as possible, but I might not be able to get it finished until a few months from now, because I also have a few commissions I'm already working on! Also, if you'd rather have a bunch of sketches over one big piece, just let me know! <3 
 > Ratings: G, Teen, Mature 
 > Universes: 616, MCU, Earth's Mightiest Heroes, Avengers Assemble, Avengers Academy, Ultimates, Marvel Adventures, Noir, 1872, 3490, Other-Fandom-Fusion, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/kayvsworld/0095/)

*****

## Art, Illustration or Comic Auction

### Creator’s Description
 > $15 - $25: black and white single-character sketch
$30 - $35: single-character sketch with flat monochromatic colours 
$40+: single-character sketch with 2 flat colours + tones 

A single character sketch of Steve or Tony! I’ll start your drawing as soon as possible, but I might not be able to get it finished until a few months from now, because I also have a few commissions I’m already working on! <3 
 > Ratings: G, Teen, Mature 
 > Universes: 616, MCU, Earth's Mightiest Heroes, Avengers Assemble, Avengers Academy, Ultimates, Marvel Adventures, Noir, 1872, 3490, Other-Fandom-Fusion, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/kayvsworld/0118/)