---
timestamp: '2017-05-13T02:19:14.446Z'
creatorName: Kelslk
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160830303310/kelslk-2017'
allCharities: null
email: kelslk.art@gmail.com
doWant: 'Mushy happy things!! '
doNotWant: 'Anything too dark or violent'
universes: null
link1: 'http://kelslk-art.tumblr.com/tagged/my-art'
link2: null
link3: null
hasCustomCharities: false
creatorTag: kelslk
category: tumblrpost
title: null
layout: tumblr
---
*****

## Art, Illustration or Comic Auction

### Creator’s Description
 > Full color drawing with no/minimal background. I'll be able to start your work in late June-early July! 
 > Ratings: G, Teen, Mature 
 > Universes: 616, MCU, Avengers Assemble, Avengers Academy, Ultimates, 3490 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/kelslk/0089/)