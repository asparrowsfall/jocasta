---
timestamp: '2017-05-12T22:10:25.758Z'
creatorName: garrideb
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160833734059/garrideb-2017'
allCharities: null
email: autumnburn@gmail.com
doWant: 'just about anything; I don''t want to take anything off the table'
doNotWant: 'graphic gore (most comic book violence is fine)'
universes: null
link1: 'http://archiveofourown.org/works?utf8=%E2%9C%93&work_search%5Bsort_column%5D=revised_at&work_search%5Bfreeform_ids%5D%5B%5D=79517&work_search%5Bother_tag_names%5D=&work_search%5Bquery%5D=&work_search%5Blanguage_id%5D=&work_search%5Bcomplete%5D=0&commit=Sort+and+Filter&user_id=garrideb'
link2: null
link3: null
hasCustomCharities: false
creatorTag: garrideb
category: tumblrpost
title: null
layout: tumblr
---
*****

## Video Auction

### Creator’s Description
 > I am willing to work as independently or as closely with the winning bidder wishes.  If you just have a vague theme or story idea, I can come up with a song.  If you have a song and a strong story concept already planned, and are mostly looking for someone with the software and expertise to make it happen, I'd be happy to work in that capacity, too!  

Please take a look at my vids, specifically my three Steve/Tony ones to see what kind of vid I can make.  Also, my Jem and the Holograms comic vid showcases some of my newest vidding tricks, so please watch that to see some additional special effects I can do.  If you have a favorite comic vid that used a style very different from mine, and want something similar to that, link me to the vid and I'll let you know if that style is within my capabilities.

I do NOT have a quick turn-around time, but I promise to keep you in the loop if you wish.  I estimate it will take me about a month to finish your vid, but it could take longer.

I'm happy to answer any questions. I want to make you a vid you'll love! 
 > Ratings: G, Teen, Mature 
 > Universes: 616, Marvel Adventures, 1872, Other-Fandom-Fusion 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/garrideb/0081/)