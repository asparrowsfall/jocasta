---
timestamp: '2017-05-13T05:18:10.352Z'
creatorName: theappleppielifestyle
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160787283514/theappleppielifestyle-2017'
allCharities: null
email: isabelle.mcneur@gmail.com
doWant: 'happy endings, hurt/comfort (if applicable), positive friendships/relationships, team as family'
doNotWant: 'explicit sex, unhappy endings, hydra!cap, mcu civil war, infidelity'
universes: null
link1: 'http://theappleppielifestyle.tumblr.com/tagged/isabelle-writes'
link2: 'http://archiveofourown.org/users/theappleppielifestyle'
link3: null
hasCustomCharities: false
creatorTag: theappleppielifestyle
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > Will write up to 5k words. Check out my work to see what kind of fic I generally write! 
 > Ratings: G, Teen, Mature 
 > Universes: 616, MCU, Other-Fandom-Fusion, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/theappleppielifestyle/0101/)