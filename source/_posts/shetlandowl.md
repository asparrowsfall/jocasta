---
timestamp: '2017-04-30T17:03:33.477Z'
creatorName: shetlandowl
charityName1: 'The Young Center for Immigrant Children’s Rights'
charityLink1: 'https://donatenow.networkforgood.org/TheYoungCenter'
charityName2: 'Planned Parenthood'
charityLink2: 'https://secure.ppaction.org/site/Donation2?df_id=12913&12913.donation=form1'
charityName3: 'Rape, Abuse & Incest National Network'
charityLink3: 'https://donate.rainn.org/'
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160717685925/shetlandowl-2017'
allCharities: true
email: how.swede@gmail.com
doWant: 'MCU, AUs, humor & fluff. Most things tbh! Happy to talk about it to be sure you get what you like. '
doNotWant: 'Sexual violence, violence against kiddos (unless it''s like a schoolyard fight and a kid pulls another kid''s ponytail), unhappy ending, infidelity. '
universes: null
link1: 'http://archiveofourown.org/users/shetlandowl/works'
link2: null
link3: null
hasCustomCharities: true
creatorTag: shetlandowl
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > The fic will be minimum 7-10k (no upper limit; I leave that to how the story unfolds), and knowing me it'll be dialogue-heavy. You can be as specific about the tropes and people you want to see as you want (and we can talk about this before writing starts, if you like!), and I'll do my best to make it happen.  
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: MCU 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/shetlandowl/0018/)