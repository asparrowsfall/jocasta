---
timestamp: '2017-05-01T22:06:27.850Z'
creatorName: Neurotoxia
charityName1: 'Amadeu Antonio Stiftung'
charityLink1: 'https://www.amadeu-antonio-stiftung.de/eng/'
charityName2: 'PRO ASYL'
charityLink2: 'https://www.proasyl.de/en/'
charityName3: 'Zentrum Demokratische Kultur'
charityLink3: 'http://zentrum-demokratische-kultur.de/spenden/'
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160730558618/neurotoxia-2017'
allCharities: true
email: shinkeidoku@gmail.com
doWant: 'angst, hurt/comfort, slice of life, established relationships, UST, pining, AUs'
doNotWant: 'pregnancy/mpreg, a/b/o, scat/watersports, character bashing, noncon'
universes: null
link1: 'http://archiveofourown.org/users/Neurotoxia'
link2: null
link3: null
hasCustomCharities: true
creatorTag: neurotoxia
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > My works tend to be around the 3-5k mark and I’m terrible at predicting word counts or working with high minimum counts. I will do my best to reflect a higher bid in my word count, but please be aware that it might not happen. 

In my work, I like to inject some angst and darkness, and I do like to make my favourites suffer, but in the end I’m a sucker for a hopeful ending at least. I can do humour and fluff, but in smaller doses. For a better look at what I like to write and read I recommend taking a look at this post, which gives a fairly detailed account on the yays and nays. Overall, I’m pretty flexible. 

As for scheduling, I might not be able to get started on your fic right away – I’m currently writing my thesis and looking for work, so until both of those are dealt with (presumably early/mid summer), fic writing might have to take a step back. However, I’ll do my best to do some development on the prompts/ideas early on and keep in touch throughout until we can get to the fun part! 
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: MCU, Avengers Academy 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/neurotoxia/0033/)