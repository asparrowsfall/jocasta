---
timestamp: '2017-05-05T23:47:45.359Z'
creatorName: Lets_call_me_Lily
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160749709131/letscallmelily-2017'
allCharities: null
email: betula.arbore@gmail.com
doWant: 'worldbuilding! plot! emotions!'
doNotWant: 'anything too kinky or explicit'
universes: null
link1: null
link2: null
link3: null
hasCustomCharities: false
creatorTag: lets-call-me-lily
category: tumblrpost
title: null
layout: tumblr
---
*****

## Other fanwork-related services, including Beta-ing, Consulting, etc. Auction

### Creator’s Description
 > Thorough beta-reading of any work of your choice - I'm great at SPaG and internal continuity, and am also happy to bounce plot ideas and check for characterisation (and happy to research things, too!). Depending on the length and my exam schedule, it may take up to a month (this is a rough estimate for epic-length fics). 
 > Ratings: G, Teen, Mature 
 > Universes: 616, MCU, Noir, 1872, 3490, Other-Fandom-Fusion 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/lets-call-me-lily/0058/)