---
id: '0117'
creatorName: Riverlander974
workType:
    desc: 'Fiction, Prose, or Other Writing'
    header_name: Fic
    slug: fic
workDescription: "For every $5 raised, I can write ~2k words for the winner. Prompt(s) up to winner.\n(Can be split into several small drabbles, or add words all up into longer fics, max ~7k per fic).\n\nIf more than $50 is raised, I can write a multi-chaptered fic, 20k-40k words. (Or you can split this down into multiple one-shots - as above).\n\nFor every additional $30 raised after $50, I can write another multi-chaptered fic, ~20k words (or work with the winner on extending a previous prompted fic). \n\nIf more than $100 is raised, I will offer a special look forward into the Carter Cousin Chronicles saga, beyond SNAFU, involving an Infinity Gem. (Winner must NOT pass this preview or any spoilers onto others. This is exclusively for the winner's eyes!)\n\nComplimentary fic banner made my me for every fic I write! :D\n\nIf you want a crossover/au, it will depend on the fusion fandom, so please check beforehand please! (Can contact me on my Tumblr). Works may take some time to complete (I'm in the middle of moving)"
minimumBid: 5
bidIncrement: 1
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160812969178/riverlander974-2017'
creatorTag: riverlander974
idString: '0117'
universes:
    - MCU
    - 'Avengers Assemble'
    - 'Avengers Academy'
    - Other-Fandom-Fusion
universeTags:
    - mcu
    - avengers-assemble
    - avengers-academy
    - other-fandom-fusion
ratings:
    - G
    - Teen
    - Mature
snippet: "For every $5 raised, I can write ~2k words for the winner. Prompt(s) up to winner.\n(Can be split into several small drabbles, or add words a..."
category: riverlander974
title: 'Riverlander974''s fic Auction'
---
For every $5 raised, I can write ~2k words for the winner. Prompt(s) up to winner.
(Can be split into several small drabbles, or add words all up into longer fics, max ~7k per fic).

If more than $50 is raised, I can write a multi-chaptered fic, 20k-40k words. (Or you can split this down into multiple one-shots - as above).

For every additional $30 raised after $50, I can write another multi-chaptered fic, ~20k words (or work with the winner on extending a previous prompted fic). 

If more than $100 is raised, I will offer a special look forward into the Carter Cousin Chronicles saga, beyond SNAFU, involving an Infinity Gem. (Winner must NOT pass this preview or any spoilers onto others. This is exclusively for the winner's eyes!)

Complimentary fic banner made my me for every fic I write! :D

If you want a crossover/au, it will depend on the fusion fandom, so please check beforehand please! (Can contact me on my Tumblr). Works may take some time to complete (I'm in the middle of moving)