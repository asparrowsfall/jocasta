---
timestamp: '2017-05-01T15:44:30.626Z'
creatorName: navaan
charityName1: 'Amnesty International'
charityLink1: 'https://www.amnesty.org/en/'
charityName2: 'International Federation of Red Cross and Red Crescent Societies'
charityLink2: 'http://www.ifrc.org/'
charityName3: 'ADD International'
charityLink3: 'https://www.add.org.uk/'
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160702925662/navaan-2017'
allCharities: true
email: navaan.lj@googlemail.com
doWant: 'pretty much anything, especially things that have hurt/comfort, identity porn, time travel, multiverse, team dynamics, canon-divergent AUs, angst, fix-its, get together, canon compliant fic'
doNotWant: 'D/s, explicit non-con/rape scenes, incest, sex with underage characters, sadism, cheating, radical AUs (high school/werewolf/vampire/genderswitch/coffee shop/A/B/O...)'
universes: null
link1: 'http://archiveofourown.org/users/navaan'
link2: null
link3: null
hasCustomCharities: true
creatorTag: navaan
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > I'm willing to write a minimum of 3k words for any of the listed universes (616, EMH, Ults, Noir 1872.) I'm good to write in any part of 616 canon from early days up to Avengers vs X-Men/pre-Hickmanvengers canon. If you want MCU or Iron Man: Armored Adventures, please contact me beforehand to discuss your idea to see if it's something I feel I can do.

I'll do my best to match your preferred tone from fluff to PWP to angsty deathfic. (For specific kinks, please ask before bidding.)

I'm happy to write multiverse crossovers and ships and sex scenes including more than one version of Steve/Tony. 

If you are interested in seeing a missing scene or sequel to one of my existing fics, I can probably work with that.

In case you want to ask something or make sure I can work with your prompt before bidding, you can reach me on tumblr at @navaan. 

At the moment I'm very busy in real life and also still working on my Reverse Big Bang, but I expect to be able to get started on your fic in early June when things calm down.
 
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: 616, Earth's Mightiest Heroes, Ultimates, Noir, 1872 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/navaan/0030/)