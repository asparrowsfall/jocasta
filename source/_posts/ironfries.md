---
timestamp: '2017-05-13T03:50:53.223Z'
creatorName: Ironfries
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160853975426/ironfries-2017'
allCharities: null
email: ironfries@gmail.com
doWant: 'I''ll draw almost anything. I''m familiar with pretty much all of the different multiverses.'
doNotWant: 'Extreme gore/torture, explicit sex.'
universes: null
link1: 'http://ironfries.tumblr.com/tagged/art'
link2: null
link3: null
hasCustomCharities: false
creatorTag: ironfries
category: tumblrpost
title: null
layout: tumblr
---
*****

## Art, Illustration or Comic Auction

### Creator’s Description
 > I'm offering an A4 sized digital illustration with minimal background. Couple pics are preferred, but I'll be willing to draw 1-3 characters. If you have any questions, feel free to contact me at @ironfries; I can let you know if I'll be able to fulfill your request. 
 > Ratings: G, Teen, Mature 
 > Universes: 616, MCU, Earth's Mightiest Heroes, Avengers Assemble, Avengers Academy, Ultimates, Marvel Adventures, Noir, 1872, 3490, Other-Fandom-Fusion, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/ironfries/0093/)