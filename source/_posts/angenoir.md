---
timestamp: '2017-05-13T13:15:11.777Z'
creatorName: AngeNoir
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160832740232/angenoir-2017'
allCharities: null
email: roletobeplayed@gmail.com
doWant: 'AUs, fusions, team iron man, fluff, domesticity, hurt/comfort, miscommunication that get resolved'
doNotWant: 'Character hatred, anything that features Loki strongly, major character death (that is permanent), terminal illness, non-con'
universes: null
link1: 'http://archiveofourown.org/users/AngeNoir/pseuds/AngeNoir'
link2: null
link3: null
hasCustomCharities: false
creatorTag: angenoir
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > Story is intended to be short (between 1k and 5k words); the month of june will be very busy for me, and I may not be able to complete the story until July. 
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: MCU, Earth's Mightiest Heroes, Avengers Academy 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/angenoir/0112/)