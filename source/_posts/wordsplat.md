---
timestamp: '2017-05-13T03:22:54.567Z'
creatorName: wordsplat
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160836053302/wordsplat-2017'
allCharities: null
email: wordsplat@gmail.com
doWant: 'Fluff, romance, happy endings'
doNotWant: 'Death, excessive angst (some is definitely okay), unhappy endings'
universes: null
link1: 'http://archiveofourown.org/users/Wordsplat/works'
link2: 'http://wordsplat.tumblr.com/tagged/wordsplat'
link3: null
hasCustomCharities: false
creatorTag: wordsplat
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > Most comfortable with 1-10k, can go longer if the plot demands. Max would be about 15-20k.  
 > Ratings: G, Teen, Mature 
 > Universes: MCU, Avengers Academy, 3490, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/wordsplat/0104/)