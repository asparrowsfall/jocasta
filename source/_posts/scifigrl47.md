---
timestamp: '2017-05-13T00:03:03.174Z'
creatorName: Scifigrl47
charityName1: 'Planned Parenthood'
charityLink1: 'https://www.plannedparenthood.org/'
charityName2: 'National Park Foundation'
charityLink2: 'https://www.nationalparks.org/'
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160848074820/scifigrl47-2017'
allCharities: true
email: sciwritesfic@gmail.com
doWant: 'fluff, found families, humor, mild angst, crack, meet cute, AUs, crossovers (provided I''m familiar with the crossover material)'
doNotWant: 'graphic violence, non con/dub con, unhappy ending, divorce, abuse'
universes: null
link1: 'http://archiveofourown.org/users/scifigrl47'
link2: 'http://scifigrl47.tumblr.com/fic'
link3: null
hasCustomCharities: true
creatorTag: scifigrl47
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > Full length, prompt driven fic.  G-M rated, as my ability with smut is rather subpar.  AUs fine, but I reserve the right to negotiate with the winner if I'm uncomfortable with the prompt.  I'll also do my best to work in any specifics that the prompter comes up with, but the story might not go in the direction that you intend.  

Exact length will be dependent on the prompt itself, but I've never been...  Concise.  I'm also not quick.  But I've managed to meet my deadlines on previous BB and RBB, so I'll have the fic done by August 1st. 
 > Ratings: G, Teen, Mature 
 > Universes: 616, MCU 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/scifigrl47/0087/)