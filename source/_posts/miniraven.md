---
timestamp: '2017-05-06T11:00:31.091Z'
creatorName: Miniraven
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160761901944/miniraven-2017'
allCharities: null
email: miniraven6791@gmail.com
doWant: 'Fluff, AU’s, romance, established relationship, and/ or get-together. Willing to negotiate'
doNotWant: 'Rape/ non-con, dub con, dom/ sub, unhappy ending, whump, pain play, or very kinky sex.'
universes: null
link1: 'http://archiveofourown.org/users/MiniRaven'
link2: null
link3: null
hasCustomCharities: false
creatorTag: miniraven
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > Willing to write a 5k word fic at max. I'm a very busy person, but I hope to finish within a month of starting the assignment. I'm also willing to negotiate tropes or kinks included in the fic on a case by case basis.  
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: MCU, Earth's Mightiest Heroes, Avengers Assemble, Avengers Academy, Other-Fandom-Fusion 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/miniraven/0065/)