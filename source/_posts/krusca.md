---
timestamp: '2017-04-30T05:46:06.827Z'
creatorName: Krusca
charityName1: '*ppl can dontae to any charity BUT ACLU (I''m salty at them because they defend "freedom of speech" for neo nazis lol)'
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160733817853/krusca-2017'
allCharities: true
email: artingkrusca@gmail.com
doWant: "- anything\n- willing to do AU/tropes of Steve/Tony from different verses (i.e. MCU soulmates AU, 616 nonpowered civilian AU)"
doNotWant: "- Hydra!Cap\n- feel free to ask just in case if unsure"
universes: null
link1: 'http://artingkrusca.tumblr.com/tagged/my-fanart'
link2: 'http://artingkrusca.tumblr.com/tagged/tbdm'
link3: null
hasCustomCharities: true
creatorTag: krusca
category: tumblrpost
title: null
layout: tumblr
---
*****

## Art, Illustration or Comic Auction

### Creator’s Description
 > Other- secret wars civil war

Also not sure if it counts as "other fandom fusion" so i left in blank but willing to do au/tropes of stevetonys from different verses (ie mcu soulmates au, 616 nonpowered civilian au)

Offering illustration- B&W or limited color palette w/minimal or no bg 
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: 616, MCU, Earth's Mightiest Heroes, Avengers Assemble, Avengers Academy, Ultimates, 1872, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/krusca/0042/)

*****

## Art, Illustration or Comic Auction

### Creator’s Description
 > Other - secret wars civil war

This is for 2 page comic, b&w (possibly limited color palette)
If amount gets to $80 or higher I'll do 3 pages, and if $120 or higher (i doubt itll get that high lol anyways) 4 pages

Bidder can submit as little or as much description of what they want in comic but final discretion for what goes on the page is still left to me. I can also draw a part from their fic or someone elses fic (i doubt any writers would be against getting art of their fic??) 
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: 616, MCU, Earth's Mightiest Heroes, Avengers Assemble, Avengers Academy, Ultimates, 1872, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/krusca/0043/)