---
timestamp: '2017-05-02T19:52:14.033Z'
creatorName: Lelantus
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160723492941/lelantus-2017'
allCharities: null
email: Lelantusfics@gmail.com
doWant: 'It''s your story and I will write pretty much anything Steve/Tony! However, some tropes I particular enjoy writing are: fake dating, memory loss, identity porn, soulmates, fix-it, hurt/comfort, fluff, and smut. Additionally, I love writing AUs and have a particular fondness for canon-divergence, non-powered, college/high school, and fantasy/medieval. A small warning: I have not attempted to or thought about writing a post CA:CW Steve/Tony fic. If that''s something you want, I''m willing to give it a shot, but I''m not sure how well it will turn out!'
doNotWant: 'Rape/non-con, incest, most of the more "extreme" kinks (bestiality, tentacles, watersports). Additionally, I would prefer not to write kidfic (includes deaging, ageplay, pregnancy, and adoption), but this one is potentially negotiable (e.g. Mpreg mentioned briefly at the end of the fic), so please feel free to contact me at lelantusfics@gmail.com if you have questions/want to talk about it further!'
universes: null
link1: 'http://archiveofourown.org/users/Lelantus'
link2: null
link3: null
hasCustomCharities: false
creatorTag: lelantus
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > For $10, I will write at least a 3K Steve/Tony fic on the prompt/topic of your choice! With regard to timing, I will be in the middle of med school applications, so the fic may take me a few weeks to write - just something to be aware of! 
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: MCU 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/lelantus/0041/)