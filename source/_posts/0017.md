---
id: '0017'
creatorName: buckmebxrnes
workType:
    desc: 'Art, Illustration or Comic'
    header_name: Art
    slug: art
workDescription: 'I will offer a custom illustration for the winning bid. It will take me 2 to 3 weeks to complete because I''m graduating law school and will begin bar prep after May 20th. I have never drawn Stony before but adore the ship and have wanted to but with commissions and fandom involvement, I''ve not had a chance yet. :)'
minimumBid: 20
bidIncrement: 5
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160715536449/buckmebxrnes-2017'
creatorTag: buckmebxrnes
idString: '0017'
universes:
    - '616'
    - MCU
    - 'Avengers Assemble'
    - 'Avengers Academy'
universeTags:
    - '616'
    - mcu
    - avengers-assemble
    - avengers-academy
ratings:
    - G
    - Teen
    - Mature
    - Explicit
snippet: 'I will offer a custom illustration for the winning bid. It will take me 2 to 3 weeks to complete because I''m graduating law school and will ...'
category: buckmebxrnes
title: 'buckmebxrnes''s art Auction'
---
I will offer a custom illustration for the winning bid. It will take me 2 to 3 weeks to complete because I'm graduating law school and will begin bar prep after May 20th. I have never drawn Stony before but adore the ship and have wanted to but with commissions and fandom involvement, I've not had a chance yet. :)