---
timestamp: '2017-05-12T19:25:22.856Z'
creatorName: Muccamukk
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160766525227/muccamukk-2017'
allCharities: null
email: muccamukk@hotmail.com
doWant: 'Canon: 616, Noir, MA:A, Next Avengers, 3490, other one shots/one-panel canon comics AUs (ask me!). Angst, tropey stuff like amnesia/sex pollen/presumed dead (etc), dubcon, pining, historical AUs.'
doNotWant: 'Canon: MCU and current 616 (I''m good up until Age of Heroes). 100% fluff, 100% Tony whump, unhappy endings/permanent character death, anything to do with Civil War (any of them).'
universes: null
link1: 'http://archiveofourown.org/users/Muccamukk/works?fandom_id=7266'
link2: null
link3: null
hasCustomCharities: false
creatorTag: muccamukk
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > Length: Something in the range of 6,000 words, but may be longer. Timing: by the end of September. I'm fine with general or specific prompts. I don't have much in the way of squicks/DNWs, but feel free to ask if I'll write something or not. 
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: 616, Marvel Adventures, Noir, 3490, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/muccamukk/0076/)