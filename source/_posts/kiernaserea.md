---
timestamp: '2017-05-13T20:32:51.578Z'
creatorName: KiernaSerea
charityName1: GEMS
charityLink1: 'http://www.gems-girls.org'
charityName2: 'Disability Rights Education & Defense Fund'
charityLink2: 'https://dredf.org'
charityName3: 'International Refugee Assistance Project'
charityLink3: 'https://refugeerights.org'
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160805407803/kiernaserea-2017'
allCharities: true
email: sereasingh@gmail.com
doWant: 'MCU, AA, fluff, angst, team dynamics, fix-its, dad! Tony fics AUs '
doNotWant: 'Rape, dub-con, betrayal, major character death, AoU unless its canon-divergent. '
universes: null
link1: null
link2: null
link3: null
hasCustomCharities: true
creatorTag: kiernaserea
category: tumblrpost
title: null
layout: tumblr
---
*****

## Other fanwork-related services, including Beta-ing, Consulting, etc. Auction

### Creator’s Description
 > Up until now I've merely written reviews and betaed for friends. I'm told I'm a great cheerleader and I'll do my best to push you to do your best without overwhelming you. I'm good with the grammar, punctuation, and spelling. I love the characters that Marvel has presented to us and I have a fairly firm and well-rounded grasp on their characterizations because of my friends. If you do want me to beta-read other universes, I'm willing to sit down and let you explain to me what you want. I'll research if you need me to. I'll mostly be free this summer. As long as you communicate with me, I'll work hard and within your schedule as much as possible. Let me know what works for you in regards to how you want me to slip in suggestions or etc. 
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: MCU, Earth's Mightiest Heroes, Avengers Assemble, Noir, 3490 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/kiernaserea/0120/)