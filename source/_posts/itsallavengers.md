---
timestamp: '2017-05-13T15:46:11.240Z'
creatorName: itsallavengers
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160819054784/itsallavengers-2017'
allCharities: null
email: itsallavengers@gmail.com
doWant: 'I don''t really mind- I''m pretty adaptable, and enjoy writing almost anything! Although I''ve always got a spot in my heart for hurt/comfort, and I love writing angst.'
doNotWant: "*NSFW \n*rape/ non con\n*major character death\n*anything where the plot is reliant on comic-canon or knowledge (I don't read them)\n*Mpreg\n*post-CW \n*Sad endings/ breakups/ cheating"
universes: null
link1: 'https://archiveofourown.org/users/itsallAvengers/works'
link2: null
link3: null
hasCustomCharities: false
creatorTag: itsallavengers
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > I may not be able to do much work this first month, due to Boring Life Stuff, but after that, I should be raring to go! 
I can write longer fics (probably 30k max though) but it will take longer, and I probably work better with shorter stuff (10/20/25k). However, I'm always up for a challenge! 
 > Ratings: Teen 
 > Universes: MCU 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/itsallavengers/0116/)