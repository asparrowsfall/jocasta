---
timestamp: '2017-05-02T09:54:03.316Z'
creatorName: avengercat
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160721803945/avengercat-2017'
allCharities: null
email: sayora347@gmail.com
doWant: 'Some beautiful PWP (I love kink prompts)! Or happy, fluffy, cute things.'
doNotWant: 'Permanent death or mutilation.'
universes: null
link1: 'http://archiveofourown.org/users/avengercat/pseuds/avengercat'
link2: null
link3: null
hasCustomCharities: false
creatorTag: avengercat
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > That PWP you've always been searching for. Or another short-to-midlength joy-inducing fanfic. 

I'll begin writing in mid-June and will aim to have it completed by the end of summer, but it may take until the end of this year as I’m also signing up for the C/IM Big Bang. 
 > Ratings: Explicit 
 > Universes: 616, MCU, Earth's Mightiest Heroes, Avengers Assemble, Avengers Academy, Other-Fandom-Fusion, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/avengercat/0037/)

*****

## Podfic Auction

### Creator’s Description
 > Claim my podfic recording cherry. A special podfic read for your pleasure. Will read up to a maximum word count of 25k.

I'll begin recording in mid-June and will aim to have it completed by the end of summer, but it may take until the end of this year as I’m also signing up for the C/IM Big Bang. 
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: 616, MCU, Earth's Mightiest Heroes, Avengers Assemble, Avengers Academy, Ultimates, Marvel Adventures, Noir, Other-Fandom-Fusion, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/avengercat/0038/)