---
timestamp: '2017-05-13T04:55:20.479Z'
creatorName: UseNecessaryForce
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160791430759/usenecessaryforce-2017'
allCharities: null
email: alyx.yohn@gmail.com
doWant: 'My preferred universe is MCU, but I''m also comfortable with Avengers Academy and EMH. TOTALLY down for queering anyone and everyone and for pretty much any AU~'
doNotWant: 'slavery/Holocaust/etc. AUs, HYDRA!Cap and/or Superior Iron Man...dark fic in general, actually'
universes: null
link1: 'http://archiveofourown.org/users/lackluster_lexicon'
link2: 'http://usenecessaryforce.tumblr.com/tagged/my-fic'
link3: null
hasCustomCharities: false
creatorTag: usenecessaryforce
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > 3k minimum. AUs will use MCU characterization unless the bidder specifies otherwise. 
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: MCU, Earth's Mightiest Heroes, Avengers Academy, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/usenecessaryforce/0099/)