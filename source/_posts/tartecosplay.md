---
timestamp: '2017-05-01T17:23:53.302Z'
creatorName: 'Tarte Cosplay'
charityName1: 'I already donate monthly to Planned Parenthood and the ACLU, so if you like, you could choose a different one! Charities benefiting refugees and education are my personal causes, if that influences your decision any.'
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160693866207/tartecosplay-2017'
modNotes: 'URL Updated'
allCharities: true
email: taaarte@gmail.com
doWant: "I excel at sadness. :)\nMaking pieces that reference other fanwork (like fanfiction, for example) is absolutely OK!\nI would prefer the commissioner have a strong idea of what they are looking for from the commission."
doNotWant: 'Pairings other than SteveTony.'
universes: null
link1: 'http://www.facebook.com/TarteCosplay/'
link2: 'http://tarteauxfraises.tumblr.com/tagged/my+mixes'
link3: null
hasCustomCharities: true
creatorTag: tartecosplay
category: tumblrpost
title: null
layout: tumblr
---
*****

## Crafts & Fanmade Merchandise Auction

### Creator’s Description
 > Custom Arc Reactor
1 custom arc reactor, in wearable form (for cosplay), table-top displayable form, or nightlight form.

- I will not be able to begin work on this until September, but once I begin work it should go fairly quickly.
- Casing plate (external design) is customizable to the auction winner’s specs (various arc reactor MKs, combination of arc reactor and shield, etc.)
- Wearable cosplay arc reactor (depending on design) will be ultra-thin, lightweight, and come with adhesive for sticking to the chest.
- Table-top displayable will come with housing for display.
- Nightlight is a wall plug-in nightlight, probably restricted to USA 2-or-3-pronged wall outlets.
- Auction winner will need to pay additional shipping to receive the prop (if this does not work, please let me know and we can work something out) 
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: 616, MCU, Earth's Mightiest Heroes, Avengers Assemble, Avengers Academy, Ultimates, Marvel Adventures, Noir, 1872, 3490, Other-Fandom-Fusion, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/tartecosplay/0035/)

*****

## Graphics, Gifsets, Edits, etc. Auction

### Creator’s Description
 > 1 SteveTony Fanmix!
Playlist will contain a minimum of 10 songs, plus mix front/back cover, and song listing with relevant quotes. The actual number of songs will almost certainly exceed 10, and will increase as the bid increases.

- Auction winner should specify mood and universe for the mix (my specialties are Sadness, Bittersweet, and Joke playlists but I will happily make any kind)
- Other specifications can include musical genre, musical artist focus, personal preference with respects to song and musical artist choice (for example, if you don’t like a specific artist, I will avoid them), etc.
- Auction winner can also specify images that they want me to use for the mix cover, within reason - if they want to use another artist’s piece, they have to clear it with them first.

Examples of my fanmixes: http://tarteauxfraises.tumblr.com/tagged/my+mixes

*I don’t know as much about Earth’s Mightiest Heroes, Avengers Assemble, or Marvel Adventures, but I’m still game to make a mix for those titles. I’m also open to making mixes for other works within the fandom, like fanfiction! 
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: 616, MCU, Earth's Mightiest Heroes, Avengers Assemble, Avengers Academy, Ultimates, Marvel Adventures, Noir, 1872, 3490, Other-Fandom-Fusion, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/tartecosplay/0036/)