---
timestamp: '2017-05-06T12:54:05.307Z'
creatorName: Domon
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160773392061/domon-2017'
allCharities: true
email: Mr.domon@gmail.com
doWant: 'any and all stevetony'
doNotWant: 'super nsfw'
universes: null
link1: 'http://tonysvandyke.tumblr.com/tagged/Domon-draws'
link2: 'http://misterdomon.tumblr.com/tagged/Domon-draws'
link3: null
hasCustomCharities: null
creatorTag: domon
category: tumblrpost
title: null
layout: tumblr
---
*****

## Art, Illustration or Comic Auction

### Creator’s Description
 > Fully rendered commission of tony and steve  
 > Ratings: G, Mature 
 > Universes: 616, MCU, Earth's Mightiest Heroes, Avengers Assemble, Avengers Academy, Ultimates, Marvel Adventures, Noir, 1872, 3490 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/domon/0068/)