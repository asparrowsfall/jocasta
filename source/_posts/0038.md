---
id: '0038'
creatorName: avengercat
workType:
    desc: Podfic
    header_name: Podfic
    slug: podfic
workDescription: "Claim my podfic recording cherry. A special podfic read for your pleasure. Will read up to a maximum word count of 25k.\n\nI'll begin recording in mid-June and will aim to have it completed by the end of summer, but it may take until the end of this year as I’m also signing up for the C/IM Big Bang."
minimumBid: 15
bidIncrement: 5
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160721803945/avengercat-2017'
creatorTag: avengercat
idString: '0038'
universes:
    - '616'
    - MCU
    - 'Earth''s Mightiest Heroes'
    - 'Avengers Assemble'
    - 'Avengers Academy'
    - Ultimates
    - 'Marvel Adventures'
    - Noir
    - Other-Fandom-Fusion
    - Other
universeTags:
    - '616'
    - mcu
    - earths-mightiest-heroes
    - avengers-assemble
    - avengers-academy
    - ultimates
    - marvel-adventures
    - noir
    - other-fandom-fusion
    - other
ratings:
    - G
    - Teen
    - Mature
    - Explicit
snippet: "Claim my podfic recording cherry. A special podfic read for your pleasure. Will read up to a maximum word count of 25k.\n\nI'll begin recordin..."
category: avengercat
title: 'avengercat''s podfic Auction'
---
Claim my podfic recording cherry. A special podfic read for your pleasure. Will read up to a maximum word count of 25k.

I'll begin recording in mid-June and will aim to have it completed by the end of summer, but it may take until the end of this year as I’m also signing up for the C/IM Big Bang.