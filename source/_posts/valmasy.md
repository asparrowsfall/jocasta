---
timestamp: '2017-05-13T20:29:03.677Z'
creatorName: Valmasy
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160810913511/valmasy-2017'
allCharities: null
email: yanceyjk@gmail.com
doWant: 'Pretty much anything'
doNotWant: 'Mpreg, watersports'
universes: null
link1: 'http://archiveofourown.org/users/Valmasy/pseuds/Valmasy'
link2: 'https://trickyarchangel.tumblr.com/tagged/my-writing'
link3: null
hasCustomCharities: false
creatorTag: valmasy
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > I am very comfortable with 5k five, one-shots or with possible add-on potential. Depending on the plot wish from the winner, I sometimes work better with vaguer requests, but can work with specific as well. 

Please have at least two ideas when bidding in case you have a plot wish I find I cannot write. I usually don’t have a problem though. If I cannot write your first choice, I’ll add an extra word limit to your fic in compensation.

I’m always available on Tumblr messaging at trickyarchangel. Feel free to contact me about ideas before bidding. 
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: 616, MCU, Earth's Mightiest Heroes, Avengers Assemble, Avengers Academy, Ultimates, Marvel Adventures, Noir, 1872, 3490, Other-Fandom-Fusion, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/valmasy/0123/)