---
timestamp: '2017-05-12T19:53:45.377Z'
creatorName: SirSapling
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160809027664/sirsapling-2017'
allCharities: null
email: SirSaplings@gmail.com
doWant: 'Pretty much anything is good, Fluff, Angst Comedy etc. '
doNotWant: 'Graphic/Explicit content such as: Sexual scenes, Nudity, Graphic depictions of violence/Injury (Light violence may be allowed). Pairings of Steve or Tony with another character in a non Gen/Shipping context (I.e I won''t draw Steve/Bucky in a romantic context, but will draw them in the same scene/ as friends if asked). Partner betrayal or abuse. Character bashing or general character hate.'
universes: null
link1: 'http://sirsapling.tumblr.com/tagged/my-art'
link2: 'https://www.instagram.com/sirsaplings/'
link3: null
hasCustomCharities: false
creatorTag: sirsapling
category: tumblrpost
title: null
layout: tumblr
---
*****

## Art, Illustration or Comic Auction

### Creator’s Description
 > $5 - $10 Simple sketch, flat Monochromatic colours
$10 - $15 More refined sketch, Monochromatic colouring and simple shading
$15 - $20 refined sketch, monochromatic colouring, simple shading and a simple background
$20 - $25 Black lined drawing, no colours
$25 - $30 Black lined drawing with monochromatic colouring and simple shading
$30 - $35 Lined drawing with flat colours. A coloured/gradient background can be added if requested
$35 - $40 Lined drawing with colour and simple hard or soft style shading
$40 - $45 Lined drawing with colour, simple shading and a non detailed background
$45 - $50 Lined drawing with colour, shading and a more detailed background.
$65+ An additional simple sketch may be requested 
 
Whilst I will endeavour to fill out any bids as quickly as possible, I may not be able to begin / complete any works until July due to Academic engagements.

Whilst I have listed Universes I am most confident with, I am happy to try others if discussed. I am happy to make fandom fusion works i.e HP!AU's or movie crossovers if I have sufficient knowledge of the subject matter/what the bidder is looking for. I'm happy to discuss what I will or won't draw before bidding at my Tumblr/ Twitter (@sirsapling) or by email SirSaplings@gmail.com. 
 > Ratings: G, Teen 
 > Universes: 616, MCU, Avengers Assemble, Avengers Academy, Ultimates, 1872, 3490, Other-Fandom-Fusion, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/sirsapling/0078/)

*****

## Art, Illustration or Comic Auction

### Creator’s Description
 > A lined drawing with colouring, shading and a background.

If bidding reaches $65+ An additional simple sketch may be requested.

Whilst I will endeavour to fill out any bids as quickly as possible, I may not be able to begin / complete any works until July due to Academic engagements.

Whilst I have listed Universes I am most comfortable with, I am happy to attempt others if discussed. For any fandom fusions, such as HP!AU or movie crossovers, I will draw them if I have the sufficient knowledge or detailed references are provided. I’m happy to illustrate fan-fiction provided it does not contradict my preferences and an excerpt is provided. I’m happy to discuss what I will and won’t draw on my Twitter/Tumblr (@sirsapling) or over email (Sirsaplings@gmail.com) prior to any bidding. 
 > Ratings: G, Teen 
 > Universes: 616, MCU, Avengers Assemble, Avengers Academy, Ultimates, 3490, Other-Fandom-Fusion, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/sirsapling/0079/)