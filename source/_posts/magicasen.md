---
timestamp: '2017-04-30T13:38:37.068Z'
creatorName: magicasen
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160682764469/magicasen-2017'
modNotes: 'URL updated'
allCharities: null
email: magicasens@gmail.com
doWant: 'Most anything goes! I especially love the multiverse, time travel, and dream shenanigans.'
doNotWant: 'non-powered or D/s AUs, poly ships (unless it''s multiple Steves and Tonys)'
universes: null
link1: 'http://archiveofourown.org/users/magicasen'
link2: null
link3: null
hasCustomCharities: false
creatorTag: magicasen
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > I will match the winning bid on this auction to the same charity the bidder donates to!

I will write a minimum of 1k words, although it'll likely be longer. I'd be happy to write spin-off/one-shot comic universes (What-Ifs, Battleworlds, etc). I'm very familiar with 616 canon from v3 until the end of Secret Wars. The rest of my reading is scattered, so just let me know what era of canon you'd like if you want 616! I'll read canon for whenever you like, it would just take a bit longer.

For multiverse crossovers, I'll write any universe as a crossover to the universes listed here, even if I haven't included them on the list. So, 616/MCU would be okay since I listed 616, but 3490/Noir wouldn't be since I didn't check off either. I'm familiar with all the major universes.  

I'm working on my RBB currently, so I anticipate being able to work on this fic from mid-June. 

If you have any questions, or if you want to see whether I can work with your idea/prompt, please send me a message at @einheriar 
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: 616, Ultimates, Avengers Assemble, Marvel Adventures, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/magicasen/0014/)