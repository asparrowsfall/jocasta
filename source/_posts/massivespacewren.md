---
timestamp: '2017-05-12T21:41:09.001Z'
creatorName: MassiveSpaceWren
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160798480170/massivespacewren-2017'
allCharities: null
email: massivespacewren@gmail.com
doWant: 'Pretty much anything. Fluff, Angst, AUs, angsty angst as long as I can headcanon a happy ending :D general prompts or specific requests are both fine.'
doNotWant: 'eye gore, too much gore in general, explicit sex, actually abusive relationship between Steve/Tony , (I''m also not too fond of them having children, or dogs)'
universes: null
link1: 'http://massivespacewren.tumblr.com/tagged/art+by+wren'
link2: null
link3: null
hasCustomCharities: false
creatorTag: massivespacewren
category: tumblrpost
title: null
layout: tumblr
---
*****

## Art, Illustration or Comic Auction

### Creator’s Description
 > I will do art for you, that means I draw or paint something at your request and send you the scan/digital picture. 
If the winning bid is under 15$ you will get a sketch,
between 15$ and 35$ means lineart or shaded pencil,
and more than 35$ will get you a fully coloured picture (probably watercolour, or digital). 

Should the winning bid be a lot more than that, we can still negotiate for more art :D
If you'd rather have more than one sketch or lineart for a higher donation, that is fine too.

I am usually busy and slow, so it may take a few months to finish. 

If you are unsure about what I'd draw, come ask or throw your ideas at me :D 
 > Ratings: G, Teen 
 > Universes: 616, MCU, Avengers Assemble, Avengers Academy, Marvel Adventures, Noir, 1872, Other-Fandom-Fusion 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/massivespacewren/0080/)

*****

## Art, Illustration or Comic Auction

### Creator’s Description
 > This auction is for grabbing some original drawings of stuff I already uplaoded. 
The winning bid will get to choose one piece of art from this overview:
http://massivespacewren.tumblr.com/image/160634181258
and additional art for every further 10$ the winning bid reaches. So you get one if the winning bid is between 10 and 20$, two arts if between 20 and 30$, three arts for 30-40$ and so on. I will pack it in an envelope and mail it to you (pretty much worldwide, if you are worried feel free to ask). Sizes of the art vary between A4 and about 9x9cm.  
 > Ratings: G, Teen, Mature 
 > Universes: 616, Avengers Assemble, 1872, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/massivespacewren/0121/)