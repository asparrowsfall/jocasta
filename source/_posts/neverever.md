---
timestamp: '2017-04-30T05:07:58.602Z'
creatorName: Neverever
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160697193060/neverever-2017'
allCharities: null
email: captainneverever@gmail.com
doWant: 'romance; love; happy endings; fluff and angst; comedy of manners; getting together; established relationship; open to writing most Steve/Tony canons; open to explicit requests; open to writing AUs'
doNotWant: 'cheating; bdsm; drug use; major character death; threesomes'
universes: null
link1: 'http://archiveofourown.org/users/Neverever'
link2: null
link3: null
hasCustomCharities: false
creatorTag: neverever
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > I am at my best writing fics of approximately 8k to 12k words; it takes about 2 to 3 months for one beta-read fic (all my fic is beta read) of that length; I am open to writing canons I haven’t done before – I’ll check about the details and expectations; I am open to writing AUs; please check my AO3 work to see what type of work I do. 
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: 616, MCU, Noir, 3490, Avengers Assemble, 1872, Avengers Academy, Earth's Mightiest Heroes, Marvel Adventures, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/neverever/0007/)