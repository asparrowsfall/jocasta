---
timestamp: '2017-05-05T23:02:15.836Z'
creatorName: Tahlruil
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160831633015/tahlruil-2017'
allCharities: null
email: cpeaty22@gmail.com
doWant: 'AU''s, angst and humor, identity porn, slash/smut'
doNotWant: 'Excessive gore, lots of action scenes, major character death, scat, non-con'
universes: null
link1: 'http://archiveofourown.org/users/Tahlruil/works'
link2: null
link3: null
hasCustomCharities: false
creatorTag: tahlruil
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > A 15k one-shot fic - I'm happy to do either stories set in the universes listed, AU's that use the particular character/background story quirks specific to one of them, or a story where two or more universes collide. For every $10 above the minimum bid, I'll add 5k words.  If bidding reaches $50, the winner can ask instead for fic with chapters with a total word count of up to 75k, or three shorter fics that can be one-shots or part of a series with the same total word count. If by some strange miracle bidding gets above $75, I'm willing to negotiate with the winner regarding word count, number of chapters or number of shorter fics. 
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: 616, MCU, Earth's Mightiest Heroes, Avengers Assemble, Avengers Academy, Ultimates 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/tahlruil/0060/)