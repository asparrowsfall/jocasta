---
timestamp: '2017-05-05T18:16:44.583Z'
creatorName: Chibisquirt
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160775261042/chibisquirt-2017'
allCharities: null
email: chibi.squirt@gmail.com
doWant: 'Upbeat endings!  Polyamory, NSFW, Science or Language Geekiness, A/B/O, and Dubious Consent (with or without the -acles) are all welcome too, although not required.'
doNotWant: 'Jealousy, extreme social awkwardness, Loki as a heroic main character, unhappy endings, Actually Evil Steve or Tony (e.g. Hydracap), or major character death.'
universes: null
link1: 'http://archiveofourown.org/users/ChibiSquirt/pseuds/ChibiSquirt'
link2: null
link3: null
hasCustomCharities: false
creatorTag: chibisquirt
category: tumblrpost
title: null
layout: tumblr
---
*****

## Other fanwork-related services, including Beta-ing, Consulting, etc. Auction

### Creator’s Description
 > I will beta- and/or cheer-read for you!  I'm told I'm pretty good at it.  I do hit the spelling, punctuation, and grammar, but I also (unless requested not to) look at dialog ("Does this really sound like Steve?"), characterization ("Wait, no, Tony *really* doesn't understand this?!"), overall plot and pacing ("You could definitely stretch this scene out, but if you do, it emphasizes it; does that serve a purpose?"), et cetera.  I prefer to beta by making edits as "suggestions" which you can accept or reject; I don't track which you do, so it's easy to reject my advice without anxiety.  While I have only seen the MCU and Marvel Noir, I am familiar enough with comics that I feel comfortable offering to beta 616 fics; I'm also happy to do other universes, it's just that you might have to explain some things to me.  Lastly, the rate:  $10 gets you a 5,000 word fic, with an additional two thousand words per dollar (so that $20 would be 25,000 words.) 
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: 616, MCU, Noir, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/chibisquirt/0054/)

*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > Other than the Likes and DNW's listed, I have very few preferences; I'm very easy-going!  If you don't see something on either of those lists, I'm probably game.  I prefer "fluff with an edge" to either treacle or darkfic, but that was probably something you could guess from the list.  (And if you really want to go dark with me as the author, I'm confused, but probably willing to work with you!)

Communication:  I primarily communicate through email for fic-purposes, because I share everything in google docs and it's just easier.  That said, you can feel free to message me on Tumblr (chibisquirt) or drop a question as a review of one of my fics on AO3, if you prefer, even before you bid.

Rate:  $15 will get a 1,500 word (or more) fic; for every $1, add 500 words.  A $20 bid would be a fic at least 4,000 words long; a $60 bid would be a fic at least 24,000 words long.  

The other kind of rate:  I can commit to writing 1,000 words per week for this, on average.  This commitment is a minimum!  If I am Very Into your prompt, it may well be faster; shorter fics also write more quickly than longer ones.  That said, when I call this a "commitment", I mean it.  I have very deliberately chosen to commit to bare minimums, here; I don't want to get in over my head, and I've put a lot of thought into making sure I don't.   
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: MCU, Noir 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/chibisquirt/0055/)