---
timestamp: '2017-05-13T04:06:57.245Z'
creatorName: dapperanachronism
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160783499333/dapperanachronism-2017'
allCharities: null
email: dapperanachronism@gmail.com
doWant: 'Angst, fluff, pining, AU''s, canon, pining, established or unestablished relationships, tropey goodness, '
doNotWant: 'Unhappy endings, major character death, a/b/o, explicit smut, '
universes: null
link1: 'https://archiveofourown.org/users/dapperAnachronism/pseuds/dapperAnachronism'
link2: null
link3: null
hasCustomCharities: false
creatorTag: dapperanachronism
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > The things on my 'will write' list are just kind of a general overview. I'm willing to write anything that isn't explicit on the 'will not write list' -- I really just want to create something that you'll enjoy! I'm offering up fic that will probably come in between 5k-10k. 
 > Ratings: G, Teen, Mature 
 > Universes: MCU 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/dapperanachronism/0094/)