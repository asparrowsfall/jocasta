---
timestamp: '2017-05-04T19:52:52.088Z'
creatorName: deusabinitio
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160753770753/deusabinitio-2017'
allCharities: null
email: mmora9@mail.csuchico.edu
doWant: 'post civil war, I don''t mind writing sex scenes, angst, fluff, abstract, character death'
doNotWant: 'rape, infidelity '
universes: null
link1: 'http://archiveofourown.org/users/KirscheLeibling'
link2: 'http://deusabinitio.tumblr.com/tagged/I-wrote-a-thing'
link3: null
hasCustomCharities: false
creatorTag: deusabinitio
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > I can start whenever the bidder likes, and will take within a week to finish. I have written very long works, but it really depends on the weight of the prompt given to me. 
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: 616, MCU, Avengers Assemble, Other-Fandom-Fusion 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/deusabinitio/0053/)