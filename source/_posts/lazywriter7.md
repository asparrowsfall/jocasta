---
timestamp: '2017-05-01T00:14:44.425Z'
creatorName: lazywriter7
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160699061051/lazywriter7-2017'
allCharities: null
email: merlinobsessed@gmail.com
doWant: 'Angst, humour or fluff, fusions, AU''s and crossovers (provided I''m familiar with the other property), characters on the grey spectrum, trope subversion, slow build and burn, other characters from Marvel verse involved, emotionally stunted characters, dysfunctional relationships, character studies.'
doNotWant: 'Smut, D/S or ABO verse, abuse, infidelity, mentioned Steve/Bucky, damsel in distress syndrome, unfair character bashing. For post CACW fic, I reserve the right to refuse aspects of a request (especially things blatantly Team Cap)'
universes: null
link1: 'http://archiveofourown.org/users/lazywriter7/pseuds/lazywriter7'
link2: null
link3: null
hasCustomCharities: false
creatorTag: lazywriter7
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > I can write in any genre, and I don't mind unhappy endings or major character death (no suicides), as long as any other Steve or Tony ship is not endgame. Am also willing to write extended scenes/codas to my previous fics, though I'd appreciate it if the request didn't go against my personal headcanon of that verse. Word count will be tailored to bidding amount. Can write up to 15k, though you never really know with that if your idea is particularly inspiring ;) I write quite slowly - it would take approximately two weeks for a 5k fic, a month for 10, and so on; though these are loose limits and I might deliver faster if my schedule frees up or a little slower if things come up. 
 > Ratings: G, Teen, Mature 
 > Universes: MCU, Noir, 3490, Other-Fandom-Fusion 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/lazywriter7/0023/)