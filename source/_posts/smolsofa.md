---
timestamp: '2017-05-13T03:02:35.714Z'
creatorName: smolsofa
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160796396547/smolsofa-2017'
allCharities: null
email: smolsofa.sony@gmail.com
doWant: 'Any canon universe, and AUs! AvAc, 616, and rule 63 are personal favorites.'
doNotWant: 'No Smut,  No Gore'
universes: null
link1: 'https://sony-rark.tumblr.com/tagged/my+art'
link2: 'https://smolsofa.tumblr.com/tagged/my+art'
link3: null
hasCustomCharities: false
creatorTag: smolsofa
category: tumblrpost
title: null
layout: tumblr
---
*****

## Art, Illustration or Comic Auction

### Creator’s Description
 > Depending on the complexity of the request, the length of time needed could vary widely, so the safe bet is that it will be finished before September. Art and illustration are my forte, and can be done in color, with shading. Short comics can also be done in color, but comics over 10 panels long will likely be in black and white. I have no one particular style, so if there was a style of mine that was desired in particular, that must be specified. If you have any questions about the content/subject of the art, or anything else, you’re very welcome to ask! 
 > Ratings: G, Teen 
 > Universes: 616, MCU, Earth's Mightiest Heroes, Avengers Assemble, Avengers Academy, Ultimates, Marvel Adventures, Noir, 1872, 3490, Other-Fandom-Fusion 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/smolsofa/0091/)