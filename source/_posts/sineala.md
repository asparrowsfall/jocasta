---
timestamp: '2017-05-01T03:31:34.530Z'
creatorName: Sineala
tumblrUrl: 'https://stonytrumpshate.tumblr.com/post/160689154744/sineala-2017'
modNotes: 'URL updated'
allCharities: null
email: sineala@gmail.com
doWant: 'Canon-focused/canon-compliant comics fic a specialty, including canon-divergent AUs and fix-its. I will write almost anything, but here are some tropes I always enjoy: amnesia, identity porn, time travel, the multiverse, historical or SF/fantasy AUs, soulbonds/soulmates, fake relationships, pining, h/c. Fluff and angst equally welcome. I will write BDSM, but contact me before bidding if you have a particular kink in mind.'
doNotWant: 'Kidfic (including deaging, ageplay, pregnancy, adoption, and Superfamily), A/B/O, mundane modern no-powers AUs, infidelity, incest, non-con, poly with characters other than multiple Steves and Tonys, underage sex, maiming/extremely graphic torture, or animal harm.'
universes: null
link1: 'http://archiveofourown.org/users/Sineala'
link2: null
link3: null
hasCustomCharities: false
creatorTag: sineala
category: tumblrpost
title: null
layout: tumblr
---
*****

## Fiction, Prose, or Other Writing Auction

### Creator’s Description
 > I am offering one story of approximately 5,000-10,000 words in length. I am willing to write in any comics-based universe (including AU miniseries and What If issues), provided that the canon material is available to me. Please note that I am both moderating and participating in the Cap-IM Big Bang, and due to that you may not receive your story until later this year. 

If you have any questions, or if you want to run your idea by me before bidding, please send me a message at @sineala. 
 > Ratings: G, Teen, Mature, Explicit 
 > Universes: 616, Ultimates, Marvel Adventures, Noir, 1872, 3490, Other 

 [CLICK HERE TO BID ON THIS WORK](http://auctions.stevetony.gives/sineala/0027/)