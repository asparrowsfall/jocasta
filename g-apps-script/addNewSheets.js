function addNewSheets() {
    var auctions = new Nimble({
        url: "https://docs.google.com/spreadsheets/d/1Zw54a_OsNaWMo6BaXLGRn6wee0-JW2RfppiyNGtqLOs/edit#gid=555419280",
        sheet: 1
    });

    var auctionJSON = JSON.parse(auctions.makeJSON());

    //THIS spreadsheet
    var db = SpreadsheetApp.openByUrl('https://docs.google.com/spreadsheets/d/1mUeUJFmXB_s3qpN2YesWodKe18AbxtG1K_iSyFhBgZo/edit#gid=1073496611');

    auctionJSON.forEach(function(auction, index) {
        var aucID = auction.creatorName.toLowerCase() + "--" + auction.id;
        var currentSheets = db.getSheets().map(function(sheet) {
            return sheet.getName() });
        if (currentSheets.indexOf(aucID) !== -1) return;

        var newSheet = db.insertSheet(aucID);
        var headers = ['timestamp', 'name', 'email', 'Over 18', 'bid', 'notify', auction.creatorName + " - " + auction.workType, auction.bidIncrement];
        newSheet.getRange(1, 1, 1, headers.length).setValues([headers]);
        newSheet.getRange('B2').setValue('Opening bid');
        newSheet.getRange('E2').setValue(auction.minimumBid - auction.bidIncrement);
        newSheet.setFrozenRows(1);
    });

}
