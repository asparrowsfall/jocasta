function doPost(request) {
    var bid = request.parameter.bid;
    var name = request.parameter.name;
    var is18 = request.parameter.ageconfirm === 'on';
    var email = request.parameter.email;
    var notify = request.parameter.notify === 'on';

    var n = new Nimble({
        url: "https://docs.google.com/spreadsheets/d/1mUeUJFmXB_s3qpN2YesWodKe18AbxtG1K_iSyFhBgZo/edit#gid=1073496611",
        sheet: request.parameter.sheet
    });

    var summary = new Nimble({
        url: "https://docs.google.com/spreadsheets/d/1mUeUJFmXB_s3qpN2YesWodKe18AbxtG1K_iSyFhBgZo/edit#gid=1073496611",
        sheet: 'summary'
    });

    var lock = LockService.getScriptLock();
    lock.waitLock(10000);

    var oldBid = n.getLastCell('bid').getValue();
    if (oldBid == 'bid') oldBid = 0;

    if (!(bid > oldBid)) return ContentService.createTextOutput(
            JSON.stringify({
                status: 'outbid',
                message: 'The bid you entered was too low. Someone may have outbid you' +
                    'while you filling out the form to submit this bid! Please refresh the page and see' +
                    'what the new current bid is.'
            }))
        .setMimeType(ContentService.MimeType.JSON);

    //outbid(n, bid, request.parameter.sheet);

    n.sheet.getRange(n.emptyStart('name'), 1, 1, 6).setValues([
        [new Date(), name, email, is18, bid, notify]
    ]);

    //confirmation(n, name, email, bid, request.parameter.sheet);

    summaryUpdate(summary, request.parameter.sheet, bid);

    lock.releaseLock();

    var status = { status: 'success' };

    return ContentService.createTextOutput(
            JSON.stringify(status))
        .setMimeType(ContentService.MimeType.JSON);
}

function summaryUpdate(summary, aucID, newBid) {
    var summaryValues = summary.sheet.getRange(1, 1, summary.sheet.getLastRow(), 2).getValues();

    var summaryListing = summaryValues.filter(function(outerArr) {
        return outerArr[0] === aucID;
    })[0];

    var row = summaryValues.indexOf(summaryListing);

    summary.sheet.getRange((row + 1), 2).setValue(newBid);

}

function confirmation(nimble, newPersonName, newPersonEmail, bid, sheetname) {
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    if (emailPattern.test(newPersonEmail) == false) return;

    var creatorName = sheetname.split('--');
    var auctionDisplayName = nimble.sheet.getRange('G1').getValue();

    var message = "Hello " + newPersonName + ", " +
        "Your bid of $" + bid + " has been successfully placed for " +
        auctionDisplayName + ".";
    var datetime = (new Date()).toLocaleString();

    var options = {
        htmlBody: "<p>Hello " + newPersonName + ",</p>" +
            "<p>Your bid of $" + bid + " was successfully placed for the <strong>" + auctionDisplayName +
            "</strong> auction. If you chose to receive notifications, we will email you if you are outbid.</p>" +
            "<p>You can also <a href='http://auctions.stevetony.gives/'>browse the complete auction list</a>.</p><p>" +
            "<a href='mailto:stonytrumpshate+bidding@gmail.com'>Contact us</a> if you have questions.</p>" +
            "<p>Thank you, <br> Stony Trumps Hate Mods</p>",
        name: "STH Mods",
        replyTo: "stonytrumpshate+bidding@gmail.com"
    };

    MailApp.sendEmail(newPersonEmail, 'Bid Confirmation:' + auctionDisplayName, message, options);
}

function rocket() {
    var n = new Nimble({
        url: "https://docs.google.com/spreadsheets/d/1mUeUJFmXB_s3qpN2YesWodKe18AbxtG1K_iSyFhBgZo/edit#gid=1073496611",
        sheet: 'asparrowsfall--0062'
    });
    outbid(n, 50, 'asparrowsfall--0062');
}

function outbid(nimble, bid, sheetname) {
    if (!nimble.getLastCell('notify').getValue()) return;

    var lastEmail = nimble.getLastCell('email').getValue();
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    if (emailPattern.test(lastEmail) == false) return;

    var creatorName = sheetname.split('--');
    var auctionDisplayName = nimble.sheet.getRange('G1').getValue();
    var notify = nimble.getLastCell('notify').getValue();
    var lastPersonName = nimble.getLastCell('name').getValue();

    var rebidURL = "http://auctions.stevetony.gives/" + creatorName[0] + "/" + creatorName[1] + "/";
    var message = "Hello " + lastPersonName + ", " +
        "You've been outbid on the auction " +
        auctionDisplayName + ". The new top bid is $" +
        bid + ". Visit " + rebidURL + " to place another bid.";
    var datetime = (new Date()).toLocaleString();

    var htmlMessage = "<p>Hello " + lastPersonName + ",</p>" +
        "<p>You've been outbid on the <strong>" + auctionDisplayName +
        "</strong> auction. The new top bid, as of " + datetime +
        " , is $" + bid + ".</p><p><strong>To place a new bid, <a href='" + rebidURL + "'>click here.</a></strong></p>" +
        "<p>You can also <a href='http://auctions.stevetony.gives/'>browse the complete auction list</a>.</p><p>" +
        "<a href='mailto:stonytrumpshate+bidding@gmail.com'>Contact us</a> if you have questions.</p>" +
        "<p>Thank you, <br> Stony Trumps Hate Mods</p>";

    // Make a POST request with a JSON payload.
    var headers = {
        "Authorization": "Basic " + Utilities.base64Encode('api:key-70d02b4a7125ae48228395581fd11508')
    };
    var data = {
        'from': 'jocasta@stevetony.gives',
        'html': htmlMessage,
        'subject': 'Outbid Notification:' + auctionDisplayName,
        'text': message,
        'to': lastEmail,
        'h:Reply-To': 'stonytrumpshate+bidding@gmail.com',
        'headers': headers
    };
    var options = {
        'method': 'post',
        'contentType': 'application/json',
        'payload': data
    };



    var reponse = UrlFetchApp.fetch(url, params);

    UrlFetchApp.fetch('https://api.mailgun.net/v3/mail.stevetony.gives/messages', options);

    /**MailApp.sendEmail(lastEmail, 'Outbid Notification:' + auctionDisplayName, message, options);**/
}
