function doGet(request) {
    var url = "1Zw54a_OsNaWMo6BaXLGRn6wee0-JW2RfppiyNGtqLOs";
    var creators = new Nimble({
        url: url,
        sheet: 0
    });

    var works = new Nimble({
        url: url,
        sheet: 1
    });

    var worksJSON = works.makeObject();

    var creatorsJSON = creators.makeObject();

    worksJSON.forEach(function(work) {
        work.creator = creatorsJSON.filter(function(creator) {
            return creator.name === work.creatorName;
        });
    });

    var worksString = JSON.stringify(worksJSON);

    return ContentService.createTextOutput(
            'assemble(' + worksString + ');')
        .setMimeType(ContentService.MimeType.JAVASCRIPT);
}
