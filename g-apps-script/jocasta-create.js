var THISURL = 'https://docs.google.com/spreadsheets/d/1mUeUJFmXB_s3qpN2YesWodKe18AbxtG1K_iSyFhBgZo/edit#gid=1073496611';

//Creates a tracking sheet for each auction in the Auction description sheet
function addNewSheets() {
    var auctions = new Nimble({
        url: "https://docs.google.com/spreadsheets/d/1Zw54a_OsNaWMo6BaXLGRn6wee0-JW2RfppiyNGtqLOs/edit#gid=555419280",
        sheet: 1
    });

    var auctionJSON = JSON.parse(auctions.makeJSON());

    //THIS spreadsheet
    var db = SpreadsheetApp.openByUrl(THISURL);

    function pad(num, size) {
        var s = num + "";
        while (s.length < size) s = "0" + s;
        return s;
    }

    var summary = db.getSheetByName("summary");

    auctionJSON.forEach(function(auction, index) {
        var aucID = auction.creatorName.toLowerCase().replace(/[\s\n\r]/g, "").replace(/[\&\/]/, "-") + "--" + pad(auction.id, 4);
        var currentSheets = db.getSheets().map(function(sheet) {
            return sheet.getName()
        });
        if (currentSheets.indexOf(aucID) !== -1) return;
        var openingBid = auction.minimumBid - auction.bidIncrement;
        var newSummaryRange = summary.getRange(summary.getLastRow() + 1, 1, 1, 2);
        newSummaryRange.setValues([
            [aucID, openingBid]
        ])

        var newSheet = db.insertSheet(aucID, db.getSheets().length + 1);
        var headers = ['timestamp', 'name', 'email', 'Over 18', 'bid', 'notify', auction.creatorName + " - " + auction.workType, auction.bidIncrement];
        newSheet.getRange(1, 1, 1, headers.length).setValues([headers]);
        newSheet.getRange('B2').setValue('Opening bid');
        newSheet.getRange('E2').setValue(openingBid);
        newSheet.setFrozenRows(1);
        newSheet.deleteColumns(9, 17);
        newSheet.deleteRows(100, 850);
    });
}

function deleteCols() {
    var dbsheets = SpreadsheetApp.openByUrl('https://docs.google.com/spreadsheets/d/1mUeUJFmXB_s3qpN2YesWodKe18AbxtG1K_iSyFhBgZo/edit#gid=1073496611').getSheets();

    dbsheets.forEach(function(sheet) {
        sheet.deleteColumns(9, 17);
    });
}

function deleteRows() {
    var dbsheets = SpreadsheetApp.openByUrl(THISURL).getSheets();

    dbsheets.forEach(function(sheet) {
        sheet.deleteRows(100, 20);
    });
}

function deleteAll() {
    var db = SpreadsheetApp.openByUrl(THISURL);
    var dbsheets = db.getSheets();

    dbsheets.shift();

    dbsheets.forEach(function(sheet) {
        db.deleteSheet(sheet);
    });
}

function refreshAllSummary() {
    var summary = new Nimble({
        url: THISURL,
        sheet: 'summary'
    });
    var summaryRange = summary.sheet.getRange(2, 1, summary.sheet.getLastRow(), 2);
    var summaryValues = summaryRange.getValues();

    summaryValues.forEach(function(outerArr) {
        Logger.log(outerArr[0])
        if (!outerArr[0]) return;
        var currentSheet = new Nimble({
            url: THISURL,
            sheet: outerArr[0]
        });
        var bid = currentSheet.getLastCell('bid').getValue();
        Logger.log(bid);
        outerArr[1] = bid;
        Logger.log(outerArr);
    });

    summaryRange.setValues(summaryValues);

}

function onOpen() {
    var ui = SpreadsheetApp.getUi();
    // Or DocumentApp or FormApp.
    ui.createMenu('Bidding Tools')
        .addItem('Refresh the summary sheet', 'refreshAllSummary')
        .addToUi();
}
