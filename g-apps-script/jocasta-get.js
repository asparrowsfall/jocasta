function doGet(request) {
    request = request || { parameters: { sheet: ['all'] } };
    if (request.parameters.sheet[0] === "all") {
        var summary = new Nimble({
            url: THISURL,
            sheet: 'summary'
        });
        var json = JSON.parse(summary.makeJSON());

        var final = {};
        json.forEach(function(row) {
            final[row.auction] = row.currentBid;
        });
        finalString = JSON.stringify(final);

        return ContentService.createTextOutput(
                'assemble(' + finalString + ');')
            .setMimeType(ContentService.MimeType.JAVASCRIPT);
    } else {
        //SINGLE-AUCTION FETCH      
        var n = new Nimble({
            url: "https://docs.google.com/spreadsheets/d/1mUeUJFmXB_s3qpN2YesWodKe18AbxtG1K_iSyFhBgZo/edit#gid=1073496611",
            sheet: request.parameters.sheet[0]
        });
        var latestBid = n.getLastCell('bid').getValue();
        var bidIncrement = n.sheet.getRange('H1').getValue();
        if (latestBid == 'bid') latestBid = 0;
        var json = JSON.stringify({
            lot: request.parameters.sheet[0],
            latestBid: latestBid,
            bidIncrement: bidIncrement
        });
        return ContentService.createTextOutput(
                'assemble(' + json + ');')
            .setMimeType(ContentService.MimeType.JAVASCRIPT);
    }
}

/*
//OLD SLOW VERSION
        //GET EVERYTHING
        var cache = CacheService.getScriptCache();
        var cached = cache.get("all-the-bids");
        if (cached != null) {
          return ContentService.createTextOutput(
                'assemble(' + cached + ');')
            .setMimeType(ContentService.MimeType.JAVASCRIPT);
        }

        var dbSheets = SpreadsheetApp.openByUrl('https://docs.google.com/spreadsheets/d/1mUeUJFmXB_s3qpN2YesWodKe18AbxtG1K_iSyFhBgZo/edit#gid=1073496611').getSheets();
        dbSheets.shift();

        var allBids = {};

        dbSheets.forEach(function(sheet) {
            var latestBid = sheet.getRange(sheet.getLastRow(), 5).getValue();
            if (latestBid == 'bid') latestBid = 0;
            allBids[sheet.getName()] = latestBid;
        });

        var json = JSON.stringify(allBids);

        cache.put("all-the-bids", json, 60); // cache for 1 minute
        return ContentService.createTextOutput(
                'assemble(' + json + ');')
            .setMimeType(ContentService.MimeType.JAVASCRIPT);
*/
